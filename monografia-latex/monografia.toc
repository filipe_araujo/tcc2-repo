\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{15}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Modelagem Bag-of-Words}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Modelagem N-grama}{18}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Modelagem Skip-grama}{19}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Algoritmo Word Vector}{20}{section.2.3}
\contentsline {section}{\numberline {2.4}Algoritmo Paragraph Vector}{21}{section.2.4}
\contentsline {section}{\numberline {2.5}Trabalhos Relacionados}{23}{section.2.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {An\IeC {\'a}lise de Sentimento com o Paragraph Vector}}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Recursos}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}Experimentos e Resultados}{26}{section.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Discuss\IeC {\~o}es Finais}}{31}{chapter.4}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Refer\^encias}{33}{section*.7}
