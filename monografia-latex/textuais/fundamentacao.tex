%==============================================================================
\chapter{Fundamentação Teórica}\label{fundamentacao}
%==============================================================================
Neste capítulo será abordado o algoritmo no estado da arte Paragraph Vector, que é uma extensão do Word Vector. Ambos os algoritmos consistem na técnica de fazer a representação distribuída de palavras e sentenças de um texto em vetores, que são estruturas que proporcionam uma melhor manipulação dos dados em tarefas de análise. É importante esclarecer que ambos os algortimos não funcionam de maneira isolada, isto é, estes trabalham em conjunto com os modelos de linguagens existentes e que já são amplamente utilizados em trabalhos recentes. Sendo assim, nas seções seguintes serão abordados modelos de linguagens que são utilizados pelo Paragraph Vector.

%------------------------------------------------------------------------------
\section{Modelagem Bag-of-Words}
%------------------------------------------------------------------------------
\par Também conhecido como modelo ``Saco de Palavras'', trata-se de um modelo muito simples, que é um dos fatores pelo qual Bag of Words (BOW) continua sendo um dos modelos mais usados. Seu funcionamento consiste em apenas contar a ocorrência de um dado conjunto de palavras de um determinado texto ou documento, sem considerar suas ordenações ou sentidos semânticos. Ou seja, a ocorrência de palavras exatamente iguais em um texto mas que possuem sentidos diferentes, serão classificadas da mesma maneira.

Para isso, define-se um dicionário com o total de palavras presentes nos textos ou documentos analisados e atribui para cada uma delas um índice que será usado nos vetores resultantes para cada um dos textos ou documentos. As posições desse vetor, que possuem como índices os valores atribuídos às palavras presentes no dicionário, armazenarão os pesos de cada palavra com aquele índice. Os valores dos pesos podem ser calculados de dois modos: binário, que simplesmente indica se determindada palavra ocorre ou não em um texto ou documento; e baseado em frequência, que indica quantas vezes uma palavra foi contabilizada num texto ou documento, como abordado por \cite{alves2010modelo}. Um exemplo do funcionamento do modelo BOW com os pesos calculados com base na frequência é mostrado abaixo:
\\
%\begin{figure}[!htb]
		%    \centering
		%    \caption{Framework para aprendizado do Word Vector}
		%    \includegraphics[scale=1]{img/bag_of_words}
		%    \label{figBagOfWords}
		%    \legend{Fonte: \citeonline{alves2010modelo}}
%\end{figure}

\textbf{Documento 1}: Pedro gosta de jogar futebol. João gosta de futebol também.

\textbf{Documento 2}: Pedro também gosta de assistir à jogos de futebol.
\\\\	
Cada palavra dos textos recebe um valor de índice, como mostrado a seguir:

\begin{BVerbatim}

			Dicionário:
			{		
			  Pedro:    1		
			  gosta:    2	
			  de:       3		
			  jogar:    4	
			  futebol:  5
			  também:   6
			  assistir: 7
			  à:        8
			  jogos:    9
			  João:    10
			}
\end{BVerbatim}

Esses índices então serão usados como entrada dos vetores de tamanho igual a 10 dos respectivos documentos:

%\begin{BVerbatim}	
%	Vetor do Documento 1: [ 1, 2, 2, 1, 2, 1, 0, 0, 0, 1 ]		
%	Vetor do Documento 2: [ 1, 1, 2, 0, 1, 1, 1, 1, 1, 0 ]
%\end{BVerbatim}

\begin{table}[ht]
  \caption{Vetores de Frequência de Palavras em um Documento utilizando BOW}
	\centering	
	\begin{tabular}{l l l l l l l l l l l}
	\\\hline 
 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\ \hline 
Vetor do Documento 1 & 1 & 2 & 2 & 1 & 2 & 1 & 0 & 0 & 0 & 1 \\
Vetor do Documento 2 & 1 & 1 & 2 & 0 & 1 & 1 & 1 & 1 & 1 & 0 \\
		\hline\end{tabular}
	\label{tabVetores}
\end{table}

Como pode ser visto, a modelagem BOW apresenta uma baixa complexidade, uma vez que apenas conta a ocorrência de palavras iguais em documentos, desconsiderando quaisquer outros aspectos relevantes, como ordenação e semântica das palavras. De acordo com \citeonline{DBLP:journals/corr/LeM14}, esses aspectos são as principais fraquezas desse modelo, o que gera a necessidade de se encontrar técnicas mais bem elaboradas à medida que se deseja obter uma melhor representação de um grande volume de palavras.

%------------------------------------------------------------------------------
\section{Modelagem N-grama}
%------------------------------------------------------------------------------
Uma alternativa à estratégia anterior é o modelo ``N-grama'' (do inglês, \textit{N-gram}), o qual elimina uma das fraquezas mencionadas na seção anterior, que é a falta de ordenação das palavras de uma dada sentença. A modelagem N-grama  consiste em criar um conjunto de tokens de uma sequência de palavras, sendo que esses tokens serão agrupados de um em um, dois em dois, e assim por diante, dependendo do valor de \textit{N}. Considerando \textit{N=2}, chamamos o modelo de Bigrama, com \textit{N=3}, temos o modelo Trigrama, e assim por diante. É possível se pensar no modelo N-grama como uma cadeia de Markov \cite{gasperin2000fundamentos}.\\

$ P(X_{t+1}=s_k|X_1,...,X_t)=P(X_{t+1}=s_k|X_t) $, sendo $ P $ a probabilidade de uma dada palavra $ X_{t+1} $ ocorrer, dado um conjunto $ X_1,...,X_t $ de palavras anteriores , apenas a primeira palavra $ X_t $ que antecede a palavra atual  é levada em consideração. Para exemplificar melhor o funcionamento da modelagem N-grama, vamos considerar os exemplos a seguir:\\

\texttt{"O filme que passou ontem na TV foi muito bom."}\\

Agrupando-se a sentença utilizando o modelo N-grama com \textit{N=1}, são obtidas as seguintes sequências:

\texttt{\textbf{1-grama} = \{ O, filme, que, passou, ontem, na, TV, foi, muito, bom \}}\\

Agrupando-se a sentença utilizando o modelo n-grama com \textit{N=2}, são obtidas as seguintes sequências:

\texttt{\textbf{Bigrama} = \{ O filme, filme que, que passou, passou ontem, ontem na, na TV, TV foi, foi muito, muito bom \}}\\

Agrupando-se a sentença utilizando o modelo n-grama com \textit{N=3}, são obtidas as seguintes sequências:

\texttt{\textbf{Trigrama} = \{ O filme que, filme que passou, que passou ontem, passou ontem na, ontem na TV, na TV foi, TV foi muito, foi muito bom \}}

Como mostrado nos exemplos, nota-se que o problema da ordenação presente nos conjuntos da modelagem BOW foi superado. Entretanto, a questão da esparsidade dos elementos ainda é presente nessa abordagem. Além disso, percebe-se que há uma distância considerável entre o sujeito da oração ``O filme'' e o seu respectivo predicado ``foi muito bom'', impossibilitando assim fazer a ligação entre os dois tokens, mesmo adotando a modelagem trigrama.

%------------------------------------------------------------------------------
\subsection{Modelagem Skip-grama}
%------------------------------------------------------------------------------
Uma outra modelagem que vem sendo bastante usada na representação de sentenças é o modelo ``Skip-grama'' (do inglês, \textit{Skip-gram}). Derivado da modelagem N-grama abordada na seção anterior, o Skip-grama surge na tentativa de contornar o problema da esparsidade dos dados presente nos modelos N-grama. De acordo com \cite{guthrie2006closer}, a modelagem Skip-grama pode ser definida pelo conjunto:\\

$ \{ w_{i_1}, w_{i_2}, ...w_{i_n} | \sum_{j=1}^{n}i_j-i_{j-1} <k \}$ \\

Ou seja, essa modelagem consiste em dar ``saltos'' de tamanho igual a \textit{k} no tradicional modelo N-grama de modo a alcançar tokens mais distantes do agrupamento feito que podem ser relevantes para a análise em questão. Desse modo, o conjunto resultante terá como elementos todas as combinações de saltos possíveis até se atinja o valor de \textit{k}. Para ilustrar melhor o funcionamento da modelagem Skip-grama, vamos considerar o exemplo a seguir:
\\\\
\texttt{"O filme que passou ontem na TV foi muito bom."}
\\\\
Agrupando-se a sentença utilizando o modelo N-grama, com \textit{N=2}, são obtidas as seguintes sequências:
\\\\
\texttt{\textbf{Bigramas} = \{ O filme, filme que, que passou, passou ontem, ontem na, na TV, TV foi, foi muito, muito bom \}}
\\\\
Considerando o mesmo modelo bigrama acima, mas agora aplicando um salto de tamanho igual a 1 (\textit{k=1}) e depois outro salto de tamanho igual a 2 (\textit{k=2}), são obtidas as seguintes sequências:
\\\\
\texttt{\textbf{1-skip-bigrama} = \{ O filme, filme que, que passou, passou ontem, ontem na, na TV, TV foi, foi muito, muito bom, O que, filme passou, que ontem, passou na, ontem TV, na foi, TV muito, foi bom \}}
\\\\
\texttt{\textbf{2-skip-bigrama} = \{ O filme, filme que, que passou, passou ontem, ontem na, na TV, TV foi, foi muito, muito bom, O que, filme passou, que ontem, passou na, ontem TV, na foi, TV muito, foi bom, O passou, filme ontem, que na, passou TV, ontem foi, na muito, TV bom \}}
\\

Como pode-se notar nos exemplos acima, o conjunto 1-skip-bigrama também considerou todos os elementos do conjunto bigramas, e o conjunto 2-skip-grama também considerou o conjunto 1-skip-grama, e assim seguem os demais conjuntos derivados do número do aumento de saltos. 

%Também pode-se notar que, nesse exemplo, o token ``foi bom'' conseguiu ser alcançado quando se aplicou a modelagem Skip-grama no modelo bigrama com salto de tamanho 1 (1-skip-bigrama), o que muito provavelmente poderá ser uma informação relevante para uma futura análise.

%------------------------------------------------------------------------------
\section{Algoritmo Word Vector}
%------------------------------------------------------------------------------
Essa técnica nos indroduz a representação distribuída de palavras em vetores, que é o foco central desse trabalho. O Word Vector (ou ``Vetor de Palavras'') consiste em estruturar cada palavra em vetores de palavras e então usá-los juntamente com modelos de linguagens apresentados nas seções anteriores. Como já foi discutido, esses modelos possuem fraquezas pelo fato de não considerarem a semântica das palavras de uma sentença, além da falta de precisão quando o tamanho das sentenças tende a crescer. Sendo assim, \cite{socher2013recursive} usaram de redes neurais com o propósito de aprender a representação de palavras em vetores para então fazer operações de multiplicação entre essas matrizes de vetores. Em trabalhos posteriores, \cite{NIPS2013_5021} mostraram que é menos custoso utilizar dos modelos Bag-of-Words e o Skip-grama no aprendizado de vetores de palavras, eliminando assim a complexidade gerada pelas camadas escondidas das redes neurais e também pela multiplicação de matrizes. Um exemplo pode ser visto na \autoref{figWordVector}.

\begin{figure}[!htb]
		    \centering
		    \caption{Framework para aprendizado do Word Vector}
		    \includegraphics[scale=0.3]{img/word_vector}
		    \label{figWordVector}
		    \legend{Fonte: \citeonline{DBLP:journals/corr/LeM14}}
\end{figure}

Sendo assim, o papel do Word Vector é maximizar as probabilidades das palavras de entrada e então fazer a predição da palavra em questão através da função de softmax hierárquico. Após passar por essa função, as palavras que possuem o mesmo sentido estarão mapeadas em um espaço vetorial, de modo que será possível  fazer a distinção semântica entre as mesmas. Por exemplo, considerando as palavras ``poderoso'', ``forte'' e ``Paris'', será possível perceber pelos vetores de cada uma delas que ``poderoso'' estará mais perto de ``forte'' que de ``Paris''. Além disso, será também permitido fazer operações algébricas entre vetores como, por exemplo, ``Rei'' - ``homem'' + ``mulher'' $ \cong $ ``Rainha''.

%------------------------------------------------------------------------------
\section{Algoritmo Paragraph Vector}
%------------------------------------------------------------------------------
Seguindo o mesmo princípio da técnica da seção anterior, o Paragraph Vector (ou ``Vetor de Parágrafos''), proposto por \cite{DBLP:journals/corr/LeM14}, é um algoritmo de aprendizado não supervisionado que surgiu como uma alternativa aos demais modelos devido a dificuldade dos mesmos em representar sentenças de palavras quando estas se tornam muito grandes, como textos e parágrafos. Além do mapeamento de palavras para vetor, mostrado pelo Vetor de Palavras na seção anterior, o Paragraph Vector também faz o mapeamento dos parágrafos para vetores distintos dos de palavras, formando uma matriz de parágrafos como pode ser visto na \autoref{figParagraphVector1}.
	
	\begin{figure}[!htb]
		    \centering
		    \caption{Framework para aprendizado do Paragraph Vector}
		    \includegraphics[scale=0.3]{img/paragraph_vector1}
		    \label{figParagraphVector1}
		    \legend{Fonte: \citeonline{DBLP:journals/corr/LeM14}}
    \end{figure}

O funcionamento do Paragraph Vector é semelhante ao do Word Vector discutido anteriormente. Em resumo, esse algoritmo possui duas etapas: o treinamento para se obter a matriz com os vetores de palavras e a matriz com os vetores de parágrafos, juntamente com os pesos de softmax hierárquico; e a predição de novos parágrafos. As vantagens de se utilizar o Paragraph Vector são a consideração da ordem das palavras de uma sentença, do tamanho variável das sentenças e da semântica, características essas que não estão presentes nos modelos anteriores. Segundo \cite{DBLP:journals/corr/LeM14}, o Paragraph Vector ``[...] É geral e aplicável à textos de qualquer tamanho: frases, parágrafos e documentos. Ele não exige ajuste de tarefas específicas da função de pesos da palavra e nem depende de árvores sintáticas''. Nesse trabalho também foi proposta uma versão alternativa do Paragraph Vector que utiliza a ideia do modelo BOW para fazer uma representação distribuída do mesmo. Sendo assim, essa versão não considera o ordenamento e o contexto das palavras de entrada para prever as palavras ao redor das mesmas na saída. Uma representação dessa versão pode ser vista na \autoref{figParagraphVector2}.

\begin{figure}[!htb]
		    \centering
		    \caption{Versão BOW distribuído do Paragraph Vector.}
		    \includegraphics[scale=0.3]{img/paragraph_vector2}
		    \label{figParagraphVector2}
		    \legend{Fonte: \citeonline{DBLP:journals/corr/LeM14}}
    \end{figure}

%------------------------------------------------------------------------------
\section{Trabalhos Relacionados}
%------------------------------------------------------------------------------
Para testar o Paragraph Vector na tarefa de AS, \cite{DBLP:journals/corr/LeM14} realizaram dois experimentos. No primeiro, foi utilizado um dataset de reviews de filmes do site Rotten Tomatoes providenciado pela Universidade Stanford\footnote{http://nlp.stanford.edu/sentiment/}. Esse córpus é constituído de 8.544 sentenças para treinamento, 2.210 para teste e 1.101 para validação. Cada sentença do dataset foi etiquetada manualmente com o sentimento presente nos reviews que variam entre muito negativo e muito positivo numa escala de 0.0 para 1.0. A tarefa então foi utilizar o Paragraph Vector em duas tarefas de classificação. A primeira delas foi considerar cinco variações de sentimento nos reviews \{ Very Negative, Negative, Neutral, Positive, Very Positive \}, e a segunda apenas duas variações \{ Negative, Positive \}. Como pode ser visto na \autoref{tabParagraphVector1}, os resultados foram satisfatórios, superando os demais modelos propostos.

\begin{table}[ht]
  
	\centering	
	\begin{tabular}{l c c}
	\\\hline 
Model & Error rate (Pos/Neg) & Error rate (Fine-grained) \\ \hline 
Naive Bayes & 18.2\%  & 59.0\%  \\
SVMs & 20.6\% & 59.3\% \\
Bigram Naive Bayes & 16.9\% & 58.1\% \\
Word Vector Averaging & 19.9\% & 67.3\% \\
Recursive Neural Network & 17.6\% & 56.8\% \\
Matrix Vector-RNN & 17.1\% & 55.6\% \\
Recursive Neural Tensor Network & 14.6\% & 54.3\% \\
Paragraph Vector & \textbf{12.2}\% & \textbf{51.3}\% \\
		\hline\end{tabular}
		\caption{Performance do Paragraph Vector comparado a outras abordagens no dataset Treebank de Sentimento de Stanford. As taxas de erros de outros métodos estão reportadas em \cite{socher2013recursive}}
	\label{tabParagraphVector1}
	\legend{Fonte: \citeonline{DBLP:journals/corr/LeM14}}
\end{table} 

No segundo experimento foi utilizado um dataset também de review de filmes do site IMDB\footnote{http://ai.stanford.edu/~amaas//data/sentiment/}, disponibilizado por \cite{Maas:2011:LWV:2002472.2002491}. A diferença entre esse córpus e o anterior é que este possui reviews compostos por várias sentenças, o que exigiu algumas mudanças no Paragraph Vector. Esse dataset é composto por 100 mil reviews de filmes do site IMDB, que são etiquetados em apenas \{ Negative, Positive \} e divididos em 25 mil instâncias de treinamento etiquetadas, 25 mil instâncias de teste também etiquetadas e 50 mil instâncias de testes não etiquetadas. Em vez de um classificador logístico linear, a utilização de redes neurais junto aos vetores de palavras se mostrou mais eficiente. Os resultados obtidos que evidenciam a superação do Paragraph Vector em relação aos demais modelos propostos em outros trabalhos são mostrados na \autoref{tabParagraphVector2}.

\begin{table}[t]
  
	\centering	
	\begin{tabular}{l c}
	\\\hline 
Model & Error rate \\ \hline 
BoW (bnc) (Maas et al., 2011) & 12.20\%  \\
BoW (bt'c) (Maas et al., 2011) & 11.77\% \\
LDA (Maas et al., 2011) & 32.58\% \\
Full+BoW (Maas et al., 2011) & 11.67\% \\
Full+Unlabeled+BoW (Maas et al., 2011) & 11.11\% \\
WRRBM (Dahl et al., 2012) & 12.58\% \\
WRRBM+BoW (bnc)  (Dahl et al., 2012) & 10.77\% \\
MNB-uni (Wang \& Manning, 2012) & 16.45\% \\
MNB-bi (Wang \& Manning, 2012) & 13.41\% \\
SVM-uni (Wang \& Manning, 2012) & 13.05\% \\
SVM-bi (Wang \& Manning, 2012) & 10.84\% \\
NBSVM-uni (Wang \& Manning, 2012) & 11.71\% \\
NBSVM-bi (Wang \& Manning, 2012) & 8.78\% \\
Paragraph Vector & \textbf{7.42\%} \\
		\hline\end{tabular}
		\caption{Performance do Paragraph Vector comparado a outras abordagens no dataset do IMDB reportados por \cite{Wang:2012:BBS:2390665.2390688}}
	\label{tabParagraphVector2}
	\legend{Fonte: \citeonline{DBLP:journals/corr/LeM14}}
\end{table}

O trabalho de \cite{Brum2015} replicou o modelo RNTN proposto por \cite{socher2013recursive} para a tarefa de AS para o idioma português brasileiro. O dataset utilizado foi o córpus ReLi disponível por \cite{freitas2012vampiro}. Como o próprio nome diz, o córpus é constituído de 12.512 sentenças resenhas de livros dividido em sentenças de polaridade positiva, negativa e neutra. Em \citeonline{Brum2015}, foram calculadas as acurácias simples, que englobam as sentenças de todas as polaridades e as acurácias combinadas, que apenas consideram as sentenças de polaridade positiva e negativa. O classifcador obteve uma acurácia simples média de 70,19\% em sintagmas e uma acurácia combinada média de 51,36\% como melhores resultados.