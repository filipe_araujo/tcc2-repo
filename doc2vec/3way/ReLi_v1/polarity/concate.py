import itertools
 
fp = (open('reli_text.txt','r'), open('reli_pol.txt','r'))
content = []
 
for lines in itertools.izip(*fp):
   content.extend(list(lines))
 
content = map(lambda x: x if x.endswith('\n') else x+'\n', content) # Colocar \n onde falta.
with open('reli_concate.txt', 'w') as dest_fp:
   dest_fp.writelines(content)
