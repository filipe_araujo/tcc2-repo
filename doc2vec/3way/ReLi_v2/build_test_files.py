#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os

"""
Esse script foi criado por Henrico Brum para aplicaÃ§Ã£o exclusiva em seu trabalho de TCC 2.
Provavelmente esse script sÃ³ funciona para esse caso. :(
Pelo menos funciona MUITO BEM pra esse caso. :)

Para usar esse script, utilize a seguinte sintaxe.

python build_test_files.py <CORPUS_PATH> <FOLDER_PATH> <FOLDER_NAME>

<CORPUS_PATH> - caminho para onde estÃ¡ o cÃ³rpus que vocÃª estÃ¡ querendo criar os arquivos de teste
<FOLDER_PATH> - caminho para onde salvar a pasta com os 10 casos de teste
<FOLDER_NAME> - nome para a pasta onde serÃ£o salvos os 10 casos de teste

Use com cuidado. NÃ£o queremos sobrescrever nada importante para vocÃª.

Exemplo:
    - python build_test_files.py /home/user/Documents/corpus.tree /home/user/Documents/ TESTES_CORPUS

SerÃ¡ criado /home/users/Documents/TESTES_CORPUS

"""


def main():
    argv = sys.argv

    if len(argv) < 4 or "-help" in argv:
        callHelp()
        exit()

    path = argv[2] + "/" + argv[3] + "/"

    # Criando o diretorio de testes
    os.mkdir(path)

    text = []

    train = []
    # dev = []
    test = []

    # Lendo arquivo de corpus
    inFile = open(argv[1], "r")

    for x in inFile:
        text.append(str(x).strip())

    inFile.close()

    total = len(text)

    print("Total de arquivos capturados: " + str(total))

    testSize = int(total / 10)
    trainSize = int((total - testSize))
    # trainSize = int((total - testSize) * .9)
    # devSize = total - (trainSize + testSize)

    print("Arvores em TRAIN: " + str(trainSize))
    # print("Arvores em DEV  : " + str( devSize ) )
    print("Arvores em TEST : " + str(testSize))

    c = 0

    while c < 10:
        for i in range(0, len(text)):
            if i >= c * testSize and i < (c + 1) * testSize:
                test.append(text[i])
            else:
                train.append(text[i])

        # Criando pasta para caso de teste
        os.mkdir(path + "/0" + str(c))
        trainFile = open(path + "/0" + str(c) + "/train.txt", "w")
        # devFile = open(path+"/0" + str(c) + "/dev.txt", "w")
        testFile = open(path + "/0" + str(c) + "/test.txt", "w")

        # os.mkdir( path+"/0" + str(c) + "/noDev", 0755)

        # trainFile2 = open(path+"/0" + str(c) + "/noDev/train.txt", "w")
        # testFile2 = open(path+"/0" + str(c) + "/noDev/test.txt", "w")

        for t in train:
            if len(t) > 0:
                trainFile.write(t + "\n")
                # trainFile2.write(t + "\n")
        # for t in dev:
        #	if len(t) > 0:
        #		devFile.write(t+"\n")
        #		trainFile2.write(t+"\n")
        for t in test:
            if len(t) > 0:
                testFile.write(t + "\n")
                # testFile2.write(t + "\n")

        train = []
        # dev = []
        test = []

        # trainFile2.close()
        # testFile2.close()

        trainFile.close()
        # devFile.close()
        testFile.close()

        c += 1


def callHelp():
    print("\nPara usar esse script, utilize a seguinte sintaxe.")

    print("\npython build_test_files.py <CORPUS_PATH> <FOLDER_PATH> <FOLDER_NAME>")

    print("\n\t<CORPUS_PATH> - caminho para onde estÃ¡ o cÃ³rpus que vocÃª estÃ¡ querendo criar os arquivos de teste")
    print("\t<FOLDER_PATH> - caminho para onde salvar a pasta com os 10 casos de teste")
    print("\t<FOLDER_NAME> - nome para a pasta onde serÃ£o salvos os 10 casos de teste")

    print("\nUse com cuidado. NÃ£o queremos sobrescrever nada importante para vocÃª.")

    print("\nExemplo:")
    print("	- python build_test_files.py /home/user/Documents/corpus.tree /home/user/Documents/ TESTES_CORPUS")

    print("\nSerÃ¡ criado /home/users/Documents/TESTES_CORPUS \n")


if __name__ == "__main__":
    main()