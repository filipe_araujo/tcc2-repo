-----------------------tags inferidas-----------------------
[ 0.  1.  0.  1.  0.  0.  0.  1.  1.  0.  1.  0.  1.  0.  0.  1.  0.  0.
  1.  1.  0.  0.  0.  1.  1.  1.  1.  0.  0.  1.  0.  1.  1.  1.  0.  0.
  1.  0.  1.  1.  1.  1.  1.  1.  0.  1.  1.  1.  1.  1.  0.  0.  0.  1.
  1.  1.  1.  1.  1.  1.  1.  1.  1.  0.  0.  1.  0.  0.  0.  0.  0.  1.
  1.  0.  1.  0.  0.  0.  0.  1.  0.  1.  1.  0.  1.  0.  1.  1.  0.  1.
  0.  0.  1.  1.  0.  0.  1.  0.  0.  1.  0.  0.  1.  1.  1.  0.  0.  1.
  0.  0.  1.  0.  1.  0.  0.  1.  0.  1.]
--------------------------------------------------------------
1 | 35 | 27
0 | 32 | 24
 0.432203 : 20 passes : dbow+dmc_inferred 0.0s 1.1s
completed pass 20 at alpha 0.002200
END 2015-11-22 16:03:42.634918
0.381356 dbow+dmm_inferred
0.398305 dbow+dmc
0.406780 Doc2Vec(dbow,d100,n5,mc2,t8)
0.406780 Doc2Vec(dm/c,d100,n5,w5,mc2,t8)
0.406780 dbow+dmc_inferred
0.406780 dbow+dmm
0.415254 Doc2Vec(dm/m,d100,n5,w10,mc2,t8)
0.440678 Doc2Vec(dbow,d100,n5,mc2,t8)_inferred
0.449153 Doc2Vec(dm/c,d100,n5,w5,mc2,t8)_inferred
0.449153 Doc2Vec(dm/m,d100,n5,w10,mc2,t8)_inferred
for doc 884...
Doc2Vec(dm/c,d100,n5,w5,mc2,t8):
 [(1176, 0.8037592768669128), (1144, 0.7197718620300293), (818, 0.6959466338157654)]
Doc2Vec(dbow,d100,n5,mc2,t8):
 [(993, 0.9922298789024353), (428, 0.9903987050056458), (950, 0.990040123462677)]
Doc2Vec(dm/m,d100,n5,w10,mc2,t8):
 [(961, 0.8358747363090515), (943, 0.8156582713127136), (483, 0.8145878314971924)]
TARGET (198): «Um livro duro , mas mesmo assim brilhante»

SIMILAR/DISSIMILAR DOCS PER MODEL Doc2Vec(dm/m,d100,n5,w10,mc2,t8):

MOST (12, 0.9102495312690735): «Um livro muito bom»

MEDIAN (333, 0.09375062584877014): «Eu li , reli e recomendo !»

LEAST (1162, -0.713183581829071): «Esse é um de os livros que mais me irrita em o mundo todo .»

most similar words for 'sempre' (13 occurences)
