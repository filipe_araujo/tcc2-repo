: Não leia achando que é um livro sobre Vampiros ou Lobsomens , pois não tem nada haver ... vampiros e lobsomens são coadjuvantes , em esta história ridícula .
Apesar de possuir um tema interessante para mim , o enredo de a história é lento e excessivamente melancólico , retratando uma personagem imbecilmente depressiva .
Li e não achei tão bom quanto parecia ser , porém , irei ler a continuação , pois como eu disse , gostei de o tema , o que não gostei foi a forma como ele foi desenvolvido em a maioria de o livro .
Talvez , porque transformar uma lenda vampiresca em um Edward que pode sair a a luz de o dia e brilha quando está em o sol meio que perde um pouco a reputação com mim .
Abandonei o livro crepúsculo por que eu gosto é de realidade , algumas coisas me fascinam outros já nem tanto e em o caso de este livro não me chamou atenção nenhuma há não ser por o nome , todos que conheceço ja leram , ja assistiram filme , e são apaixonados por crepúsculo eclipser enfim .
Mais uma coisa que não me dá prazer nenhum é histórinhas de vampiros , que são gente , que amam enfim é como se meu cachorro falasse com mim e viesse a passar ser meu namorado , nada contra mais também nada a favor .
História nada surpriendente e nada inovadora .
Trama de péssimo gosto .
História supervalorizada .
Crepusculo é um livro legal , pra se ler sem compromisso e uma unica vez , é uma história que podia ter acabado ai , porque a cada continuação a história foi ficando pior , é meio inexplicavel porque a série é um fenomeno , mas é até interessante , leia sem pretensões
Empolgante , porém fraco .
Um começo empolgante , porém vai perdendo o ritmo o que o torna fraco , além de ser muito parado quase nada acontece as situações são mal descritas por a autora as vezes ela se perde em as descrições .
Resumindo : Um começo bom , desenvolvimento regular e final ruim .
Comecei a ler Crepúsculo com uma certa reserva , achei a história estranha quando ouvi falar de ela .
E. depois de lê - lo , posso dizer : realmente não é tão bem escrito , nem a história é tão boa .
Principalmente em a primeira parte de o livro , tudo é descrito a exaustão .
Gosto não se discute e ponto final , os fãs que me perdoem , mas Crepúsculo foi uma decepção imensa .
Foi a primeira vez em a vida que me deixei levar por a capa de o livro e por a divulgação e acho que nunca me arrependi tanto em uma compra .
A ideia de o enredo é boa , quem pecou foi a escritora .
Mas perde pontos por a perfeição de Edward , tudo bem que é por que é em a narrativa de a Bella , mas cansa tanta beleza e frases pomposas ... Bom , por o público alvo , é um bom livro .
Esse é um de os livros que mais me irrita em o mundo todo .
Em a minha opinião , Crepúsculo é um livro mal escrito que tem um sucesso injustificável .
Nem a história nem a estrutura de o texto me pareceram bem trabalhados .
Bella é apaixonadíssima por Edward e passa 80 % de o livro louvando a beleza de ele , o que é algo beeeem chato .
Os vampiros de o livro não são nada legais , vampiros que não são vilões - isso até suporto - e que praticamente não tem fraquezas físicas - sol , alho , crucifixo , estaca , etc - .
Acho que este livro não merece nem mesmo uma estrela .
Em a verdade , não consegui nem ler o primeiro capítulo , porque a história é muito chata e , para piorar , a personagem principal - Bella - é muito fútil e superficial .
Apesar de ter ação , é um romance bem ' água com açúcar ' e bem meloso .
A plot não é assim tão interessante e emocionante , mas para ordinary teenagers , é algo de encher os olhos ... até uma segunda leitura .
O grande pecado de Stephenie Meyer se deve a o fato de , em alguns momentos , a história se tornar irritantemente repetitiva .
As declarações exarcebadas de Bella a o amor incondicional e irremediável também me soaram muito exageradas , e inumeras partes ficaram ' melosas ' demais pra minha glicose .
Bella também é ' meio ' chatinha e Edward muito ' perfeitinho ' .
Esperava mais .
O livro realmente só começou a prender minha atenção de a metade para o final , em que Edward e Bella começam a viver a paixão proibida .
Esperava um pouco mais de o livro , não em o sentido de a história ou sobre o enredo ... refiro - me a o conteúdo sobre os vampiros . . Buscava novas informações , as antigas tradições e até um pouco mais de sedução .
Respeito a criatividade de a autora , mas confesso que fiquei desapontada em relação a o comportamento de Edward .
Não vou dizer que é o livro perfeito porque não é .
Forçada , evidentemente -- mas hello !
Stephanie até arranhou o eterno dilema AnneRiceano - '' eu me alimento de seres vivos , mas não quero ser um monstro '' - , mas só arranhou , acho que se ousasse se aprofundar mais , perderia o tom de '' adolescentes de o bem '' que tentou dar a os seus personagens , em esta aventura puramente comercial .
Romance adolescente bonitinho e meloso que poderia ter acabado em o primeiro livro .
Mais vazio impossível .
É um livro extremamente vazio , baseado apenas em um romance - desculpem os que gostam - patético entre uma garota retardada e um vampiro - ?
- chato e vegetariano .
Suas longas e idênticas descrições de a beleza de Edward chegam a cansar os olhos .
Os personagens não atraem e o livro torna - se a cada página mais tedioso e raso .
O livro é apenas uma simples fórmula de sucesso pré-adolescente : garotos '' irresistíveis '' , um amor que supera tudo e muita atração sexual .
Não há nenhuma profundidade .
Mas me decepicionei .
A narrativa é boa e a Stephenier tem criatividade , só que o amor obssesivo de a Bella chegou a me deixar doente .
Nem terminei de ler .
Consegui ler até o final embora a Bella seja uma protagonista chata e cansativa .
Com certeza não continuarei a ler os demais exemplares , tendo tantos outros livros fantásticos a serem desvendados .
Faltam a os personagens as idiosincrasias necessárias para que se possa enxergar pessoas reais em eles .
A paixão desmedida de Bella , que prefere a morte a ficar longe de Edward , chega a ser ridícula , em certos pontos .
Horrível .
Sim , concordo que a Bella é chata .
Primeiramente devo dizer que o livro é uma pancada em a gramática inglesa .
Em segundo lugar , a obra mais me parece uma de aquelas fanfictions esdrúxulas de adolescentes que sonham com um namorado perfeito .
Por fim , personagens que não se desenvolvem , cenas de sexo sem nenhuma responsabilidade que resulta em um monstro-doce-garota-de-nome-terrível que cresce inacreditavelmente rápido .
A história se desenrola sem praticamente nenhuma ação .
Há poucos acontecimentos relevantes .
A protagonista , várias vezes , me irritou , mostrando - se um tanto depressiva , queixando - se de tudo , o tempo todo .
A autora usa algumas repetições que aborrecem , como dizer o quanto o vampiro é perfeito - de duas em duas páginas .
Não lerei as continuações de esta saga .
Quem sabe a linguagem audio-visual compense a carência literária .
Crepúsculo é um livro razoável .
Nada extraordinário como sua fama , mas ainda assim um livro '' médio '' .
Não pensei que ele fosse taaão juvenil ... !
Não mereceu o sucesso que fez
Em o entanto não merece nem 10 % de o sucesso que fez .
A história de os 4 livros poderia ter sido resumida perfeitamente em um só .
'' Crepúsculo '' é como um cachorro correndo atrás de o próprio rabo : obsessivo e inútil .
Os personagens são ridículos , rasos e mal construídos ; a fascinação que o casal principal exerce é oriunda de a falta de profundidade de ambos e o relacionamento doentio é deplorável e sem sal .
Muito chatinho , de a sono em umas partes e outras são muito bobas .
Olivro se torna muito previsivel e a linguagem é muito pobre ...
Não gostei !
extremamente imaturo , livro pra criancinha mesmo ...
Ok , eu definitivamente não gostei de o livro .
Como romancezinho , ele é até bonitinho ... mas , fala sério !
Escritora amadora , livro muito fraco onde os personagens são estáticos , estória cliche e fraca e uma repetição enorme de palavras .
Quando terminei de ler , fiquei com a sensação de '' Nossa ... Eu li ' isso ' ? ''
O livro é bom , mas eu reafirmo : Eu li os clássicos , como Drácula e Entrevista com vampiro e Vampiro Lestat , então é muito difícil assimilar um vampiro que brilha em o sol a o invés de queimar até morrer ...
Inicialmente o livro é um pouco monôtono , muito detalhista e repetitivo .
Também acho que a autora - ou talvez a tradutora , não sei - poderia ter escrito melhor algumas partes , pois pra mim tem frases muito mal compostas .
Não achei muito bem escrito , para falar a verdade .
Não pode ser considerado livro
De início , eu gostei , até engolir todo o açúcar e perceber que os personagens eram incrivelmente ruins .
A história em si é a a base de mais um clichê de a humanidade : vampiro bom se apaixona por garota que tem algum poder especial ou que é simplesmente intrometida .
O enredo , como disse , também é falho , porque apenas fala de uma idéia de amor que revela perseguição e obsessão e só .
Se ele fosse um vilão , isso seria simplesmente maravilhoso , porém ele é tão irritante quanto a garota que narra a história inteira .
Mas Bella se vê presa por aquele vampiro-que-não-vira-pó e , como se em um passe de mágica , se apaixona por esse ser falho .
Os outros personagens também são mal explorados .
Acredito que ele não pode ser considerado livro porque não passa de uma idéia repetitiva , cheia de clichês e partes surreais e que se torna mais obsessivo e menos interessante com o passar de o tempo .
Recomendável para seu pior inimigo , que vai se apaixonar por uma idéia clichê e muito açucarada que o fará passar mal .
Apesar de eu achar algumas falhas ... Muito bem bolado , arrebatador !
Crepúsculo é um livro meloso , muitas vezes entediante , machista e com personagens mal desenvolvidos ... TALVEZ , se Crepúsculo fosse só um romance sem vampiros e lobisomens fosse até um bom livro .
De forma geral , eu gostei .
Não achei um espetáculo e muito menos ruim .
Médio .
Fraquinho fraquinho ... O romance não tem nada de interessante , sendo uma fórmula batida e nem bem utilizada .
Bella é uma personagem muito plana , sem graça e previsivel .
E o Edward nem personalidade tem , é uma casca bonita .
O enredo é ruim e a autora quis dar algo diferente em ele jogando o fator '' vampiresco '' mas isso prejudica ainda mais o livro , pois o torna ridiculo , a autora deturpa as lendas de as criaturas de a noite .
Eles se apaixonam a primeira vista e não há nenhum desenvolvimento em esse aspecto .
O problema todo está em os diálogos ; irresistivelmente previsíveis .
Mas tem uma '' falha '' que deixa o livro , em alguns momentos , entediante : o excesso de pensamentos de Bela , é possível '' pular '' algumas paginas e continuar lendo sem nenhum problema .
Apesar de o jeito leve e fácil de escrever de a autora , o livro peca em diversos pontos .
Os personagens não são realmente desenvolvidos e o vampirismo é apenas um pano de fundo qualquer para uma paixonite adolescente .
Péssimo =P !
Descrições fracas , personagens sem personalidade .
O amor obsessivo é outra coisa falha em o livro .
Uma estrela apenas .
Depois de atravessar as 380 páginas de essa meleca , tive que tomar café pra virar macho .
Mas me enganou certinho .
Predicados demais ... algumas vezes me pareceu exagero ... por outro lado em os dá a descrição perfeita de personagens e lugares .
Quando começei a ler estava com uma grande perspectiva pois todos diziam que esse livro era ótimo , mas confesso que me decepcionei ... o início é muito parado , só a partir de a metade que foi melhorando .
Esperava mais de esse grande sucesso .
Gostei de o livro , leitura simple , mas esperava mais emoção e ação a história é muito parada .
Mercenária .
Em o entanto , qdo li o livro , vi que não passava de uma autora inexperiente , que escreve mal , utiliza clichês sem piedade e a protagonista é insuportavelmente fraca , irritante e rasa demais .
Superficialidade resumiria bem esse livro .
Bom particularmente como admirador de histórias de vampiros achei extremamente fraco .
Achei meramente comum meloso e chato tecnicamente é mal escrito geralmente histórias de vampiros tem um certo tom de suspense e misticismo acho o romance entre os dois muito mal explorado .
o livro é interessante mas para adolescentes achei futil bobinho .
Depois a idéia começa a ficar piegas demais .
Não acredito que perdi tempo tentando encontrar algo de bom em isso .
Porém , não aquela maravilha , aquela perfeição que todos falam ... Talvez por eu estar acostumada a ler , Crepúsculo é fraco .
A autora ainda peca em vários outros pontos , o principal de eles , em a minha opinião , foi ter deixado toda a aventura e suspense para o fim de o livro .
E falha um pouco em a descrição de a cidade , cenário para a maior parte de a história , além de enfatizar que é nublado , muito nublado , enevoado , verde , muito verde e sem sol . . Fica dificil se interessar mais por o lugar .
Esse livro , se você for parar para avaliar criticamente , em a verdade , não tem nada de muito especial ... E uma historia fantasiosa , envolvente , mas é ura ficção e todo mundo sabe de isso . . Mas , como explicar o fascinio que causa em quem le ?
Muito mamão com açúcar ñ é o meu estilo de livro ... . eu comprei os três primeiros livros juntos ... espero que os outros tenham um pouco mais de sangue .
Decepcionante .
História clichê , recheada de coisas impossíveis até em o mundo de o maravilhoso , escrita péssima , personagens mal-construídos e alienados .
Romancezinho adolescente água-com-açúcar , ridículo .
O livro mais bobo que já li .
Personagens cansativos , previsíveis , a Bella é a menina mais chata e enjoada que já existiu em a literatura .
Vampiro bonzinho pra mim não dá , não dá mesmo .
Achei o livro muito repetivo .
Mas quando chegou em o meio , meio que me desanimei , tanto é que levei 3 meses para ler .
DEPRIMENTE !
Sem ofensa para os que lera , mas ... esse livro é totalmente , debilmente despojado de qualquer forma de sublimidade ou cultura .
A narração é horroroza , muito teen para o meu gosto .
Simplesmente não engoli .
Parei em a metade de o livro .
Escrita fraca , enredo ridículo , personagens sem personalidade e uma protagonista altamente Mary Sue .
O plot de esse livro é fraco , não existe desenvolvimento de os personagens secundáros , a escritora se repete milhares de vezes - eu não aguentava mais ler sobre o peitoral másculo de diamante de o Edward !
- e apresenta um romance adolescente absurdo e opressivo como se fosse o ideal de amor perfeito .
Me decepcionei com o romance que em o começo parece ser inútil pro desenrolar de a história , mas em o final fui percebendo ... O amor é o extremo de todos os lados de o governo extremista multilateral .
A história é boa , mas não é um livro que conseguiu prender minha atenção .
Eu mesma fiquei bem confusa com a leitura e sobre as comparações que o autor teve a intenção de fazer .
Em certos momentos tive tanto ódio de algumas coisas que preferia até parar de ler para não explodir de estresse .
A primeira metade dolivro é simplesmente entendiante .
Por várias vezes pensei em abandonar .
A idéia política de o livro é boa , mas o autor é muito repetitivo , o livro nao tinha nenhum gancho que prendesse a atençao .
Em o entanto , acho que o apelo reacionário que ele tinha se perdeu em o mundo em que vivemos hoje , revelando suas falhas como literatura .
Apesar de um bom livro , a meu ver , 1984 tem mais valor histórico de o que literário e , quando observado com calma , é bastante forçado e manipulador .
Eu acho que este livro teria sido infinitamente melhor se Orwell tivesse pesado menos a mão , mas talvez a ideia de o autor tenha sido deixar bem claro sobre o que estava falando .
O primeiro é enfadonho , apenas uma longa e detalhada descrição de a realidade em a qual o personagem está inserido e algo sobre ele .
A ideia de o último capítulo é um balde de água fria , apesar de ser maravilhosamente escrito .
Porque apesar de ser um livro com uma história muito boa e interessante , ele é extremamente monótono .
A verdade é que 1984 não tem uma história pra contar .
E é apenas isso que o livro nos mostra , como o mundo vai ser se continuar indo por o caminho que vem seguindo , ou ia seguindo antes de 1984 .
Me senti completamente sufocada !
- não leria de novo ! -
PONTO NEGATIVO : A linguagem utilizada é um pouco pesada para pessoas não habituadas a leitura .
Foi tãããão díficil terminei este livro , e como o DVD estava em casa eu pensei '' vou assistir o filme pra me incentivar a terminar '' , e foi aí que a coisa piorou !
Os pensamentos de as personagens e as filosofias de o autor são falados , repetidos , re-repetidos , re-re-repetidos ... E em o final de o livro ainda há uma grande redação reforçando tudo mais ainda .
Porém a história é cansativa , infelizmente .
Quem não aprecia tanto , talvez não tenha gostado muito , principalmente de aquelas trinta páginas que falam de a política vigente de o partido .
Reconheço que esta foi a única parte de o livro em que fiquei um pouco decepcionado , pois como um amigo meu me falou , ele poderia muito bem tê - la resumida , pois essa parte interessava sim , mas nem tanto .
Tem uma grande história , mas infelizmente eu não tenho saco pro estilo de escrever de o autor
muito violento !
Em o entanto , urge fazer certas ressalvas : como toda narrativa que se propõe fazer um diagnóstico de o seu tempo e de a sociedade , tentando até antecipar em certos pontos o futuro , deixa a desejar em muitos pontos .
Contudo ... Desesperança até demais !
Não há a luz em o fim de o túnel .
Não há nenhum tipo de vitória de a virtude em suas páginas .
Essa é a narrativa de o vazio e de o niilismo totais .
Li esse livro em o meu tempo de colégio e até hoje me lembro como em o começo foi muito difícil a leitura .
Em a verdade em o começo estava achando insuportável o livro .
Lá por o fim , a leitura deixa de ser interessante .
Confesso que não compraria , até porque não gostei de ' Fala sério , amor ! ' .
É um livrinho pra meninas adolescentes , é bonitinho , engraçadinho , conta coisas por as quais todas as meninas acabam passando , mas senti falta de uma linha , de um clímax antes de o desfecho .
Bem enjoadinho !
Li em poucos dias e rápido !
Ainda bem que foi bem baratinho .
Um fato que me incomodou muito foi que a Malu - protagonista - troca de namorado como se trocasse de roupa , a impressão que me passou foi que homens são seres descartáveis e que por qualquer briga termina e pronto .
Mesmo sendo um livro de '' contos '' isso me incomodou .
Foi o primeiro livro que eu li de a Thalita Rebouças e como todas as pessoas que eu conheço elogiavam muito a autora , fui com grande expectativa para esse livro e , admito , me decepcionei um pouco .
Mais não é só isso , esse livro gera muitas decepções a os leitores , que como eu curto um bom livro .
Fala serio , amor narra de uma forma simples e pacata o começo de as descobertas mais esperadas de toda garota , Um livro feito para o publico jovem deveria , de fato , ser escrito com mais paixão , com mais emoção , com a inocência que se é perdida , de a cumplicidade que se tem com o garoto que você escolha pra dar seu primeiro beijo .
Esse livro não é nada mais de o que a historia de uma adolescente boba , que se achava a a esperta , ou seja , um livro sem historia , sem verdade e com muita bobagem .
Péssimo .
Meio chato , meio bobo , meio argh ...
É interessante , mas , de fato , não está nem entre os 50 melhores que eu já li .
São mini contos muito mal elaborados e meio bobos , o que contribui para o ar de falsa simpatia que o leitor cria por a história .
Definitivamente , irritante .
Enquanto lia esse livro , não é exagero dizer que tive que respirar fundo e contar até dez inúmeras vezes para não largá - lo por a metade .
O problema em '' Fala sério , mãe ! '' , é o mesmo de '' Fala Sério , professor ! '' : pouca conexão com a realidade .
Mas em este ocorre com mais frequencia , o que torna a leitura algo irritante , em alguns momentos .
Pode - se notar claramente esse surrealismo observando as falas de Malu quando pequena .
Muito vocabulário para pouca idade , como em essa frase , de quando ela tinha 6 anos : '' - Uma beleza enorme , mamãe .
Ou em a insistência em saber porque seu nome é Maria de Lourdes , não se conformando com as respostas de a mãe , sempre replicando com comentários assustadoramente complexos para uma menina de 5 anos .
A mãe é demasiadamente sem-noção .
O problema é que não funcionou , justamente porque as atitudes são tão fora de o comum , que a o invés de ser engraçado , soa forçado , artificial , e até mesmo ridículo , algumas vezes .
Achei o livro um pouco fútil .
Acho que adolescentes como a Malu são difícies de encontrar , não me identifiquei com ela .
O que achei meio estranho foi a maneira de como a filha trata a mãe em muitos momentos , como ela é respondona , como já com tão pouca idade tem vocabulário extenso para respostas assustadoras ... Mas depois fiquei pensando ... isso é estranho para mim , pois nunca vivi isso em casa .
Mas a partir de a segunda metade , Malu começa a ' ` pagar mico ' ' a usar o vocabulário aborrecente de ela e isso não me agradou .
Mt bobo , indicado pra adolescentes e só .
Confesso que , em o início , não fui muito com a cara de a história .
Mais o mais insuportavel comportamento de o pai de Malu foi de quando ela começou a jogar futebol , seu pai fala que isso não e coisa de menina é que ela esta errada , mais Malu e munto boa em o futebol e ensina ate para seu pai que mesmo que ela seja uma mulher ela pode ter os mesmo direitos e as mesmas abilidades para jogar futebou como um homem qualquer !
Embora o livro seja legal , não gosto de a literatura de a Thalita Rebouças , acho que os livros de ela deveriam ser menos engraçados e mais interessantes , pois um livro tem que te prender , aí que é bom mesmo .
É esse tipo de bobagem que as editoras estão impondo a os pré-adolescentes ?
Então , animada resolvi começar por o de o professor , e infelizmente me decepcionei muito .
A o contrário de o que imaginei , não me identifiquei nada com os professores de a Malu , e muito menos com ela .
Achei o livro muito artificial , e nada próximo de a realidade de os estudantes .
Li quando tinha 12 anos , e acredite , em essa idade já achei o livro infantil demais .
Não vale a pena
É um livro chato e sem uma narração muito boa .
Diferente de os outrso de a série , ele não te prende a a estória e te insentiva a abandonar o livro .
Eu só terminei de lê - lo por falta de o que fazer .
é meio chato o fato de que ela não amadureçe .
Eu achei este livro muito infantil e só consegui de ler porque tenho meio que uma política de começou então termina .
Ele não conseguiu me prender direito , achei a história bem sem sal , já cheguei a ler outro livro de a autora , o livro Fala Sério Mãe e este cheguei a achar engraçadinho .
Porém , não é um livro que você lê esperando saber o que vai acontecer quando acabar o livro , como a maioria , por isso me decepcionei com o final .
Muito hype por nada ...
Todo mundo fala que esse livro é foda , mas em a verdade ele é um lixo .
O resto , é sofrível enquanto literatura .
Não sei como uma porcaria de essa conseguiu ser consagrada .
Acho que esse eh o livro mais chato que ja li em a vida .
Um personagem completamente desagradavel , em a minha humilde opiniao .
Muito por nada
Pois bem , que decepção .
Não sei se não capturei a essência de o livro , mas a cada página lida mais crescia minha vontade de abandonar a leitura , mas esperava q melhorasse , e não melhorou .
Livro bobo , cujo personagem principal é um idiota .
Não que seja ruim , longe de isso , mas é que esperava mais ' profundidade ' , digamos .
Acho que o fato de ele terminar o livro tão perdido quanto começou que foi um pouco frustrante .
Não passa de um livro razoável , e apenas isso .
Não recomendo sua leitura A A quem não encontra - se com uma vontade e curiosidade grandiosa acerca de o mesmo - acredite , eu estava muito curioso e com vontade de desvendá - lo , por isso o li rapidamente , porém findei por exausto e um tanto decepcionado .
Por outro lado , o que determinou os contras abomináveis de o livro , fora justamente a sua pobreza em a história em si ; situações que não prendem sua atenção e tampouco causam - lhe preocupação acerca de o que acontecerá em os próximos capítulos - é morno demais !
De tão simplista , torna - se chato e sem tesão .
Em o começo , achei meio parado e pensei que o Holden fosse um adolescente como qualquer outro .
Não acho incrível , mas ouvi dizer que é título obrigatório .
Não é um livro ruim , mas , me desculpe quem não partilha de a minha opinião , não é tudo isso que dizem .
Algumas partes são , inclusive , enfadonhas e , para mim , que sempre tive que lutar muito , me pareceu mais o choro de uma criança mimada .
E não vai se decepcionar como eu , se não esperar demais .
Até hoje não compreendo porque esse livro é considerado um clássico de a literatura .
A começar por o título de o livro que nada tem a ver com a história de o livro .
Pois é . . belo exemplo para a juventude esse '' clássico '' de a literatura .
Bem acabou o livro e a historia não ficou legal .
O livro chega a ser deprimente , mas o humor negro que já citei de o Holden resgata o livro de uma total tristeza .
Hoje em dia é só mais um livro .
Protagonista chato que dói .
Em as primeiras páginas veio a decepção !
Que linguagem chula era aquela ?
Que personagem irritante era aquele ?
Era uma história fraca demais para ser tratada como clássico ou '' coisa que o valha '' .
Acho que dei apenas 4 estrelas por conta de a linguagem mais que informal e as vezes que foram repetidas as palavras '' deprimente '' e '' droga '' em esse livro .
Isso me deprimiu xD
Não gostei
Não gostei muito .
Falou - se tanto de a obra quando de a morte de o autor em Janeiro de 2010 que eu esperava um pouco mais .
Toda hora ele diz que algo o deprime e essa repetição é que deprime a gente .
Mas o enredo não me cativou nem um pouco , terminei o livro naquelas , por obrigação .
Enfim , eu achei ele bem sem graça .
Sem graça mesmo .
É meio angustiante a violência que ele tem , misturada com uma babaquice masculina juvenil e uma certa infantilidade .
Mas confesso que '' O apanhador em o campo de centeio '' não mexeu muito com mim não .
A repetição de algumas expressões me irritou durante a leitura .
É um livro bem escrito , mas sempre parece que algo está prestes a acontecer e NADA acontece .
Me frustrei .
O livro não foi tudo que eu esperava , mas acho que comecei a lê - lo com as expectativas erradas .
Eu não gosto de o título , que é uma tradução literal .
O fim poderia ser melhor , mais desenvolvido .
Por o menos eu esperava mais .
As ' aventuras ' que teve em o caminho de casa , foram até interessante , foi o que movimentou o livro , mas sua raiva extrema , foi quase que insuportável .
E em o fim , achei um livro ' qualquer ' e nao um livro que ' todo mundo ' devia ler em a sua adolescência -LSB- isso foi o que alguém comentou sobre o livro -RSB- .
O livro , de início , não empolga muito .
