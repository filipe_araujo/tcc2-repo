Confesso que li este livro apenas porque tenho que fazer um trabaho de a escola sobre ele , entretanto eu fiquei surpresa com a história .
A o contrário de esses livros " lançamento " , Capitães da Areia é sensível , emocionante e não é óbvio .
Você fica o livro inteiro torcendo por os meninos abandonados que moram em a orla marítma de Salvador .
E chorando , sinceramente .
Um romance trágico vem em a metade de o livro para acabar de vez com qualquer dúvida que você tenha sobre a qualidade de o livro .
Pontos Positivos : Além de a história ser tudo de bom , ela faz uma grande critica a o capitalismo que exclui os mas necessitados .
Isso não atrapalha muito em o decorrer de o livro , mas em o final a repetição de informações faz uma confusão ... Tem que prestar atenção para conseguir entender o final !
Confesso que chorei muito com a tragetória de os garotos que se denomiavam Capitães de a Areia .
Suas vidas sofridas e todo acontecimento que acarreta em um final triste e pesaroso , mas impressionante e belo .
Sem sombra de dúvidas um de os meus livros favoritos , pois acarreta emoção e mostra uma realidade distante , mas tão presente em a vida de todos .
Livro bem escrito , com algumas cenas fortes e tocantes .
Acho que foi o livro de escola que mais gostei !
Porque é simplesmente poesia , uma poesia triste que se desfarça de alegre .
Lembro que eu sonhava com aqueles meninos , que sempre que eu via uma telha brilhando em o sol , lembrava de eles e de a ' cidade de a Bahia ' .
Até hoje invejo sua liberdade .
O Sem-pernas era meu predileto - : Recomendo pra quem quer se divertir e nao tem vergonha de chorar .
" Sinopse tirada de Livros Faculdade Título : Capitães da Areia Autor : Jorge Amado Ano : 1937 Livro especular escrito por Jorge Amado .
Em o entanto , eu ainda me prendo em a idéia de que Dora , a única Capitã , tem um grande destaque em o livro , por ser ela a causadora de uma mudança em cada um de os integrantes de o grupo .
Sinceramente , eu me apaixonei por essa obra .
Mas fui surpreendida .
O tema é incrível , assim como os personagens são fascinantes .
Os personagens são cativantes em seu jeito , em suas atitudes , em seus pensamentos , escritos por Jorge Amado de uma maneira que te dá uma sede enorme por mais palavras .
Não há como largar esse livro antes de terminar .
Uma história muito forte , " Capitães de a areia " entrou em circulação em a época de o Estado Novo de Vargas , a censura reprimiu duramente o livro , queimando milhares de exemplares e proibindo sua venda .
Uma história que retrata fielmente a realidade de os meninos de rua , não só de a Bahia , mas de o país como um todo .
Grande tesouro de a literatura brasileira .
Vale a pena ler !
É uma história comovente que leva - nos a questionar qual o papel de cada um em a sociedade .
Tão antigo e tão atual
Apesar de ter sido escrito em 1937 , Capitães da Areia retrata muito bem a realidade de as crianças que vivem em as ruas , abandonadas a própria sorte .
Aprendemos muito com Capitães de a Areia .
Esse é o livro !
Adoro já li umas 3 vezes e sempre tem aquele gostinho de Bahia , gostinho de Salvador ... qdo ando em as ruas de o centro de a cidade ou para o lado de a cidade baixa , sempre encontro o espírito dpos meninos de Jorge .
A melancolia feliz de o povo brasileiro .
Confesso , que tinha que ler pra escola , e se eu não tivesse que ter lido pra ela , eu provavelmente não leria esse livro que me fez aprender muito com este , e viver , chorar e rir com os personagens .
De fato , alguns são , mas a história de esse livro é tão viciante que realmente não importei .
O jeito que os personagens são ladrões mais vítimias a o mesmo tempo , traz uma crtíca enorme a o governo brasileiro , que mostra que os vigaristas só são o que são devido a o governo .
A história é linda e arranca lágrimas , de um jeito brasileiro .
Sem falar de um romance maravilhoso .
É inevitável traçar um paralelo com a vida real , os meninos e suas ações são chocantes , mas bastante razoáveis dada a realidade de eles .
Me fez pensar muito .
Amei o livro
Capitães de a areia , é uma obra extremamente fantastica .
Simplesmente amei a obra , muito perfeita , desliciosa de ler , tirando alguns momentos de narrativa " chata " .
Este é um livro que talvez todo adolescente deveria ler , pra enfim agradecer de a vida que tem .
Fabuloso !
Capitães da Areia com certeza é um de os meus livros prediletos , é um livro que consegue agrupar romance , drama e acão .
E principalmente é um livro que não tem pessoas copletamente boas e nem ruins .
Comecei a leitura com um grande preconceito de a história , mas com o desenrrolar de o livros as palavras e a história de Jorge Amda conseguiu me cativar de um jeito que eu não esperava .
Terminei o livro com lágrimas em os olhos e uma vontade imensa de ir correndo para o norte de o país ajudar essas crianças .
Com o passar de o tempo e a intimidade com as letras , acabei começando a leitura de este livro ainda muito criança e mesmo sem entender muita coisa me divertia fazendo uso de esta obra que sabe Deus a quem pertencia .
Após vários anos decidi ler pra valer , com consciência de o conteúdo e claro , adorei .
Além de ser um clássico de a literatura , acabou tornando - se um clássico de minha história .
Adorei o Pedro Bala , que era o chefe/líder de os Capitães de a Areia .
Ri muito de as coisas e de o jeito que ele tratava seus ~companheiros~ de o trapiche ... de o Sem-Pernas então .. aaah foi de cortar o coração ele lá com a dona Ester .. e em o elevador D : ele era meio revoltado mas eu gostava de ele .
adorei o livro ! x
Meu predileto .
