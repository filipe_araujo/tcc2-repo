De a forma como foi escrito o livro acaba tornando - se um pouco enfadonho e repetitivo , e o entusiasmo fica menor com a seqüência de o desenrolar de a história .
O final é ridículo !
Detestei o livro por causa de isso .
Fora que a história não tem clímax .
Não recomendo .
Para você curtir o livro , você tem que superar algumas " barreiras " , a primeira é que o livro esta em português de Portugal , então tem algumas palavras que tem que ser deduzidas por o contesto .
A segunda barreira é que Saramago escreve com poucos parágrafos , o que A as vezes torna a leitura um pouco cansativa , os diálogos são indiretos , mas não atrapalha em nada a historia , de a pra entender perfeitamente as discussões , mesmo sem travessão , você percebe quem ta falando o que .
A única coisa que eu não gosto em ele é a escrita um tanto diferente por o autor ser português .
Só achei cansativa a leitura pois esta em português de Portugal .
Apesar de muitas vezes ser uma leitura dificil , por causa de o tipo de linguagem usado , de os poucos paragrafos , de as falas sem travessões , apenas separadas por virgulas ou ponto final .
Eu esperava bem mais de Ensaio Sobre a Cegueira .
Esperava um livro fantástico e impressionante , mas em o fim acabei bem decepcionado por conta de alguns motivos .
Por fim Saramago adora enrolar , o livro é repleto de frases de 8 , 9 linhas que não dizem nada , sendo simplesmente uma sequência de palavras que não adicionam nada - por exemplo : " O primeiro de a fila de o meio está parado , deve haver ali um problema mecânico qualquer , o acelerador solto , a alavanca de a caixa de velocidades que se encravou , ou uma avaria de o sistema hidráulico , blocagem de os travões , falha de o circuito eléctrico , se é que não se lhe acabou simplesmente a gasolina , não seria a primeira vez que se dava o caso " - , assim como inúmeros trechos que não adicionam nada A A história .
O ritmo de a história é um tanto quanto inconsistente , em o geral é tudo meio lento e talvez por o volume de texto tem - se a impressão de que a coisa não está evoluindo .
Os personagens principais são muito mal desenvolvidos , interpreto isso que o autor queria colocar o foco em o ' todo ' , mas é um tanto quanto estranho acompanhar um grupo de personagens tão simples .
E a conclusão de a história me pareceu um tanto quanto preguiçosa , pra não dizer meio que vazia , afinal muito de o propósito de todos os acontecimentos acabaram se perdendo em o desfecho .
Nunca sofri tanto para ler um livro .
Mas , realmente , depois de ter lido , Memorial de o Convento , A jangada de pedra e outros mais de o Saramago , tenho a dizer que esse livro me atormentou e não voltarei a lê - lo tão em breve !
Em o entanto , acho que criei muita expectativa e quando o final realmente chegou , não achei tudo aquilo ... Para mim , foi só bom .
Confesso que em o começo não gostei .
A falta de pontuação ás vezes me confundiu por o fato de não saber se era a personagem quem estava falando ou o narrador .
Em uma palavra : chato .
O que chega a os dois únicos pontos negativos de o livro : 1º é a questão de que os capítulos são imensos , quase que intermináveis , o que deixa a leitura mais lenta , pesada , massante .
Outro ponto negativo é exatamente isso , em certos pontos fica dificil de prosseguir com a leitura , o livro fica muito massante .
Mas confesso que este não está em a lista de os meus livros preferidos de ele .
Mas achei o final mto triste , não gostei não .
E também não podemos esquecer o Larry Douglas , que eu particularmente detesto , mas isso só vocês lendo para entender .
Não é uma obra prima literária com certeza , mas Sidney Sheldon tinha um dom que poucos tem : ele sabia como contar uma história e principalmente uma boa história ... Este é um de os melhores livros que li em minha vida .
Chato , entediante , repetitivo
Desisti de todos os demais títulos , não consegui terminar a leitura .
O enredo é fraco e nada bem escrito , deixou muito a desejar .
Li por ler , emprestado de um amigo .
Com todo o respeito a quem gostou , mas sinceramente não consegui entender o que faz esse livro ser considerado bom .
Achei a narrativa pobre , a história muito forçada e extremamente mal contada , os personagens " murchos " .
Insisto : com todo o respeito a quem gostou , mas esse livro é desprezível .
Não sei se recomendaria esse livro , penso que não com tantas opções em o mundo .
Como disse em o título , o início de o livro não anima muito , pareceu bastante arrastado .
Hoje a leitura fluiu mais rápida , mas depois de ler tantos livros de o Vonnegut e de tantos outros autores , eu acho o Sidney Sheldon muito prolixo e as vezes os mistérios são meio previsíveis .
Ruim , uma estória muito previsível , como em todos os livros que ele escreve .
Kate pode ser inteligente , mulher de negocios , linda ... mas parece que nao tem coração , passa por cima de td e de todos por a Empressa !
Odiei o que ela fez e as consequências que isso causou em a vida de pessoas que nasceram inocentes e que , tenho certeza , não teriam sido corrompidas se Kate não tivesse feito o que fez .
Mas não daria um ' dez ' a um livro cujo personagem , pra atingir seu ' final feliz ' , controlou , manipulou e destruiu vidas em o processo .
Em o inicio eu fiquei com a impressão de ser um plágio de O Conde de Monte Cristo , mas só a primeira parte é parecida .
Mesmo assim , achei algumas partes de a história previsíveis e o final um tanto quanto tosco .
Você tem uma protagonista burra que dói que faz uma besteira atrás de a outra , acaba presa por um crime que não cometeu e entra pra faculdade de a malandragem em a penitenciária .
Parece legal , mas dá sono .
Agora , os personagens , os relacionamentos e os diálogos são muito fracos .
Os personagens passam a se amar e a se desamar de uma hora pra outra e eles são completamente volúveis e superficiais .
Não é um romance como pretende ser e nem é original ; é uma coletânea de contos sendo vários de eles pirateados , inclusive um de escritor brasileiro .
O início de a história em nada se relaciona com o meio ou o final .
O livro é envolvente e tal , te prende em o começo , mas de o meio pro final parece meio forçado , o autor meio que começa a inventar histórias demais , não havendo uma ligação coerente entre elas , fica uma impressão de que se está lendo algum tipo de livro seriado , sei lá , com várias histórias diferentes em o mesmo livro .
Se não houver coisa melhor
Se Houver Amanhã não é tão ruim quanto eu imaginava , é claro que o livro não é um 5 estrelas , a prosa é pró lixo , superficial de mais , não permitindo que vc se apegue a os personagens e torça por eles .
Apesar de as cinco estrelas , o livro possui alguns pontos fracos : A história em si é um pouco exagerada , principalmente por as barbaridades que acontecem em a vida de Tracy .
O fato de a narrativa ser muito dinâmica e de não se abster em uma mesma situação por mais que algumas poucas páginas , faz com que alguns trechos se tornem ainda mais exacerbados .
Enredo mirabolante , trama envolvente mas chega a haver exagero .
nao chega a ser o melhor romance .... Gostei de o livro ... Não é uma historia maravilhosa mas ele te prende bastante ... so q alguns roubos de a protagonista foram sem muita explicação ... mas vale a pena le - lo .
