De a forma como foi escrito o livro acaba tornando - se um pouco enfadonho e repetitivo , e o entusiasmo fica menor com a seqüência de o desenrolar de a história .
O final é ridículo !
Detestei o livro por causa de isso .
Fora que a história não tem clímax .
Não recomendo .
Para você curtir o livro , você tem que superar algumas '' barreiras '' , a primeira é que o livro esta em português de Portugal , então tem algumas palavras que tem que ser deduzidas por o contesto .
A segunda barreira é que Saramago escreve com poucos parágrafos , o que A as vezes torna a leitura um pouco cansativa , os diálogos são indiretos , mas não atrapalha em nada a historia , de a pra entender perfeitamente as discussões , mesmo sem travessão , você percebe quem ta falando o que .
A única coisa que eu não gosto em ele é a escrita um tanto diferente por o autor ser português .
Só achei cansativa a leitura pois esta em português de Portugal .
Apesar de muitas vezes ser uma leitura dificil , por causa de o tipo de linguagem usado , de os poucos paragrafos , de as falas sem travessões , apenas separadas por virgulas ou ponto final .
Eu esperava bem mais de Ensaio Sobre a Cegueira .
Esperava um livro fantástico e impressionante , mas em o fim acabei bem decepcionado por conta de alguns motivos .
Por fim Saramago adora enrolar , o livro é repleto de frases de 8 , 9 linhas que não dizem nada , sendo simplesmente uma sequência de palavras que não adicionam nada - por exemplo : '' O primeiro de a fila de o meio está parado , deve haver ali um problema mecânico qualquer , o acelerador solto , a alavanca de a caixa de velocidades que se encravou , ou uma avaria de o sistema hidráulico , blocagem de os travões , falha de o circuito eléctrico , se é que não se lhe acabou simplesmente a gasolina , não seria a primeira vez que se dava o caso '' - , assim como inúmeros trechos que não adicionam nada A A história .
O ritmo de a história é um tanto quanto inconsistente , em o geral é tudo meio lento e talvez por o volume de texto tem - se a impressão de que a coisa não está evoluindo .
Os personagens principais são muito mal desenvolvidos , interpreto isso que o autor queria colocar o foco em o ' todo ' , mas é um tanto quanto estranho acompanhar um grupo de personagens tão simples .
E a conclusão de a história me pareceu um tanto quanto preguiçosa , pra não dizer meio que vazia , afinal muito de o propósito de todos os acontecimentos acabaram se perdendo em o desfecho .
Nunca sofri tanto para ler um livro .
Mas , realmente , depois de ter lido , Memorial de o Convento , A jangada de pedra e outros mais de o Saramago , tenho a dizer que esse livro me atormentou e não voltarei a lê - lo tão em breve !
Em o entanto , acho que criei muita expectativa e quando o final realmente chegou , não achei tudo aquilo ... Para mim , foi só bom .
Confesso que em o começo não gostei .
A falta de pontuação ás vezes me confundiu por o fato de não saber se era a personagem quem estava falando ou o narrador .
Em uma palavra : chato .
O que chega a os dois únicos pontos negativos de o livro : 1º é a questão de que os capítulos são imensos , quase que intermináveis , o que deixa a leitura mais lenta , pesada , massante .
Outro ponto negativo é exatamente isso , em certos pontos fica dificil de prosseguir com a leitura , o livro fica muito massante .
Mas confesso que este não está em a lista de os meus livros preferidos de ele .
Mas achei o final mto triste , não gostei não .
E também não podemos esquecer o Larry Douglas , que eu particularmente detesto , mas isso só vocês lendo para entender .
Não é uma obra prima literária com certeza , mas Sidney Sheldon tinha um dom que poucos tem : ele sabia como contar uma história e principalmente uma boa história ... Este é um de os melhores livros que li em minha vida .
Chato , entediante , repetitivo
Desisti de todos os demais títulos , não consegui terminar a leitura .
O enredo é fraco e nada bem escrito , deixou muito a desejar .
Li por ler , emprestado de um amigo .
Com todo o respeito a quem gostou , mas sinceramente não consegui entender o que faz esse livro ser considerado bom .
Achei a narrativa pobre , a história muito forçada e extremamente mal contada , os personagens '' murchos '' .
Insisto : com todo o respeito a quem gostou , mas esse livro é desprezível .
Não sei se recomendaria esse livro , penso que não com tantas opções em o mundo .
Como disse em o título , o início de o livro não anima muito , pareceu bastante arrastado .
Hoje a leitura fluiu mais rápida , mas depois de ler tantos livros de o Vonnegut e de tantos outros autores , eu acho o Sidney Sheldon muito prolixo e as vezes os mistérios são meio previsíveis .
Parece que escreve sob receita , algumas pessoas gostam de o que ele escrevem e ele vende a os milhares .
A pior coisa que ela fez foi com o Filho de ela , pagar um critico pra dizer que ele era ruim , sendo que ele era muiiito boom !
É um história ótima , ótima mesmo .
Kate mereceu todo o sofrimento por o qual passou .
PARECIDA apenas .
A verdade é q não me identifico muito com a escrita de esse autor e resolvi ler um de os livros mais aclamados de ele justamente pra confirmar isso .
Quando ela sai , vai procurar vingança .
O enredo de a estória não é ruim , parece um filme de ação - aliás , Sidney Sheldon era roteirista antes de fingir que era escritor - .
Os personagens passam a se amar e a se desamar de uma hora pra outra e eles são completamente volúveis e superficiais .
Em um filme , acho que você nem perceberia .
Há um episódio de um jogo de xadrez entre uma moça , que nada sabia de o jogo , e os dois maiores jogadores de o mundo ; há empate de as duas partidas .
A história realmente vira a história em a página 170 . . .
Não é a melhor obra de o grande Sidney Sheldon , mas recomendo , uma boa leitura pra passar o tempo .
Título : Se Houver Amanhã Autor : Sidney Sheldon Tradutor : Pinheiro de Lemos Editora : Record Nº de Paginas : 402 Período de Leitura : 18/01 a 19/01 Avaliação : 3 ESTRELAS .
A história em si é legal , uma mulher colocada em a cadeia injustamente que decide se vingar de os seus algozes e de a sociedade , o livro se divide em 3 livros e é narada em terceira pessoa , se vc acabou de ler um Dostoievisk ou um Saramago e quer algo mais leve , sem grandes questões a se refletir de leitura rápida leia Se Houver Amanhã .
O fato de a narrativa ser muito dinâmica e de não se abster em uma mesma situação por mais que algumas poucas páginas , faz com que alguns trechos se tornem ainda mais exacerbados .
Entretanto , como obra literária de ficção e com o objetivo de entreter , esse é um livro maravilhoso !
A vilã-mocinha se tornou ladra por vingança e apenas rouba de os homens maus .
grande passa tempo . . li rapidinho ... e envolvente tambem ... vc nunca vai acha - lo chato ou cansativo ... ele eh bem corrido ... leiammmmmmm
