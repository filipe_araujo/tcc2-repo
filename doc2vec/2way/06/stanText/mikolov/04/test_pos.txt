Ja li 22 vezes .
Amei .
Simplesmente amei .
Jorge Amado narra a história de meninos de rua em Salvador e emociona com as reviravoltas de o destino para cada um de eles .
Chama a atenção o estilo simplista de narrativa , sem floreios gramaticais ou léxicos , mas que só eleva a idéia por trás de a obra .
Comovente e marcante .
Capitães da Areia encanta por a doçura e brasilidade .
Um livro que entristece , choca e permanece em a memória .
Capitães da Areia é um retrato vívido de a realidade de os órfãos de a capital bahiana naquela época .
O estilo de ele é bem solto , fácil e a a vontade e gostei bastante de a cadência sensual de a narrativa .
A sensação que ela passa é a de um gingado com as palavras , como um rebolado natural .
Os personagens são bastante reais , possuem motivações próprias e permitem que você sinta empatia de os conflitos de meninos que não querem nada mais de o que o amor de uma família .
Recomendado !
É incrível a forma de escrita de Jorge Amado .
É impressionante como , a cada vez que retomava o livro , eu me transportava imediatamente para a Bahia .
Ele em os puxa , parágrafo a parágrafo , para o ambiente de estória .
Já em este primeiro livro , virei fã de carteirinha .
Com personagens ricos como Pirulito , que sonha ser padre , Volta Seca que deseja lutar a o lado de seu padrinho Lampião , Professor que é alfabetizado e pintor , Sem Pernas um aleijado que odeia a tudo e a todos , Gato um jovem estiloso e trambiqueiro , Boa-Vida um malandro que deseja viver em Salvador dedicando - se a boêmia e mantendo a maior distância possível de qualquer espécie de trabalho .
Belíssimo livro .
Emocionante .
O livro me surpreendeu , não esperava tanto .
Muito bem escrito , uma história muito interessante que chegou até minhas mãos - ou olhos - através de a escola .
Apesar de ter achado o livro bom , bem como excelente crítica social presente até hoje , não foi um livro que pessoalmente me agradou muito .
Vale a pena como conhecimento .
Capitães de a areia é um livro envolvente , um romance de primeira , uma obra-prima de o nosso primoroso Jorge Amado !
Ele me ensinou a ver o outro lado de a moeda quando eu ainda brincava de boneca .
Coloquei como objeto de desejo porque por incrível que parece ainda não o tenho em minha estante , mas é um livro que vive em meu coração .
É um misto de emoções onde o leitor ri , chora , tem raiva , amor , coragem , medo , se sente criança em o corpo de adulto .
Lindo e triste como a nossa juventude é largada , é esquecida por as ruas e vielas .
Eu adorei esse livro quando li , foi um conselho de um garoto que é escritor eu conheci em a internet , foi ótimo .
A hitória que se passa em as ruas baianas , em que eles roubam a estilo Robin Hood é bem interessante , como garotos tinha que cuidar uns de os outros para viver , o amor juvenil , aprender a lidar com a morte , perdas , separações e muitas outras coisas antes de o tempo é triste , mas é interessante descobrir como algumas crianças viviam naquela época e , eu achei , que o livro é muito bem escrito .
Um de os melhores livros que já li em a vida .
Nunca pensei que uma recomendação de uma professora de o colegial me trouxesse a emoção a o extremo .
Cheio de nuâncias em emoções .
Em esse livro eu ri , chorei , tive raiva , dó ... todos os sentimentos possíveis , mais me prendi a ele e creio que todos de lerem irão fazer o mesmo .
Além de isso , em uma passagem brilhante de o livro , um de os capitães , Pirulito , o mais religioso de eles , encerra um dilema diante de uma imagem de o menino Jesus em uma loja .
Isso tudo temperado por o pano de fundo ideológico , que durante alguns anos - até a série '' Os Subterrâneos de a Liberdade '' , publicado em os anos 50 - seria ingrediente de a receita de o autor , mas aqui , a o contrário de o que aconteceria em outros livros , não enfraquece o texto , por o contrário , o torna ainda mais veemente .
Sua prosa fácil e direta - influência de sua experiência jornalística ? - elegem '' Capitães de a Areia '' como uma de as principais referências literárias adotadas por as escolas de todo o Brasil .
Merece o posto .
Um soco em a boca de o estômago
É uma crítica social bem forte , a o mesmo tempo que é lírico e triste .
De subversivo a o estado a a profundo , esse livro é invariavelmente foda .
Leitura Obrigatória , SIM .
Infelizmente só descobri o encantavel livro '' CAPITÃES DE AREIA '' a os 17 anos .
Poderia jurar que esse livro foi escrito esse ano , pois é um assunto tão atual , e com uma leitura tão agradavel , que passaria fácil como atual .
Tenho que confessar , torci por cada um de eles , me apeguei a todos os personagens como se fossem meus amigos , pessoas conhecidas .
Adorei o breve porém encantador romance entre Pedro e Dora -LSB- de a qual eu chorei com o final ocorrido a ela -RSB- , a todas as fugas , os roubos , os dramas e a tristeza que cercava aquele grupo .
Me apeguei tanto a eles , que por vezes achava inocente os atos de eles , não por não saberem o que faziam , mais sim por falta de opção , por falta de alternativa .
Enfim , uma leitura agradável , fácil , e encantadora .
Leitura Obrigatória SIM , porque livros como esse merecem ser lidos .
Indubitavelmente , o melhor livro que já li .
Acho incrível a forma com a qual o livro é conduzido , apresentado e descrito .
Só posso dizer : quero ler de novo !
Interessante : a verdade nua e crua de os meninos de rua , um clássico que pode ser lido e contextualizado a a nossa realidade .
Me emocionei com todos os garotos e seus sonhos , todos mesmo .
Claro que me apaixonei por Dora também - garota que servia de tudo um pouco para eles , uma família que nunca tiveram .
Mas apesar de tudo , até que eu gostei d' este livro .
