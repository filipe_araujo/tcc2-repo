Uma realidade que não deve ser muito diferente hoje em dia .
Sou um fervoroso defensor de o sexo feminino e desprezo qualquer coisa que relacione violência contra a mulher e o livro mostra um pouco de isso .
Jorge Amado escreve de forma envolvente , e em os faz sentir vontade de ajudar cada um de os personagens .
Meu livro preferido !
Realidade crua e lírica
Em os últimos dias de 2011 , os meninos de Jorge Amado , um por um , tirou - me de a zona de conforto e , por a mão , me conduziu por as ruelas de a Salvador de os anos 1930 ; conduziu - me a uma realidade nua , crua e atual , mas a o mesmo tempo bela e lírica .
Vale a pena também assistir o filme '' Capitães de a Areia '' , dirigido por Cecília Amado , neta de o escritor baiano .
Eu de verdade não sabia o que esperar de o livro .
Digo , não sabia nem a sinopse antes de começar a ler .
Engraçado que foi em poucos momentos que eu me situava em a década de trinta , porque é evidente que o livro não se tornou menos realista com o passar de os anos .
Não dá pra não olhar a o redor e pensar diferente , nem que seja um pouquinho .
De história de verdade .
Mesmo que as vezes seja difícil acreditar que o livro fala de crianças , de adolescentes mais novos que eu .
O que só reforça a mensagem de o livro .
Que a pobreza , o desprezo e a desigualdade tiram tudo de os capitães , inclusive a infância - menos a liberdade , que '' a liberdade é o bem maior de o mundo '' - .
Jorge Amado não possui de as técnicas de escrita mais eruditas , mas o que se propôs a fazer em esse livro o fez com bastante precisão .
A crítica social evidente a cerca de a '' cidade de a Bahia '' e seus meninos de rua soa bastante atual , além de mostrar o lado de aqueles que são considerados apenas '' moleques '' .
O enredo conta a história de os Capitães de a Areia : crianças que abandonaram suas famílias - alguns não tinham família - e foram morar em um trapiche , onde vivem de furto para sobreviverem .
A história ocorre em a Bahia , de crianças pobres em que tem ajuda de o Padre José Pedro , em que um dia fora alertado de retirar sua posição por ter ajudá - los ; e Don ` Aninha , a mãe de santo de os Capitães .
E o mais valente de o grupo , seria Pedro Bala , atual chefe de os Capitães de a Areia , é a cabeça de o grupo , tanto em decisões como esquemas de roubos .
-LSB- ... -RSB- - continuar quando terminar de ler -
Um passado presente
Os capitães são crianças quando veem um carrossel ou lembram de suas mães a o receberem o carinho de Dora , uma menina-moça cheia de maturidade e coragem , que é levada junto a o bando empurrada por a morte .
Sem-perna é criança quando se depara com uma mãe de coração despedaçado , que o abraça , o veste e o ama como o menino que só ela enxerga .
Pedro é criança quando escuta as histórias sobre o seu pai e a sua imaginação voa por não saber exatamente por o que ele lutava .
Os capitães são adultos quando roubam , estupram e trapaceiam .
Quando quase todo o grupo ignora as lágrimas de uma menina .
Quando se arriscam para salvar uma imagem de um de os seus orixás .
Quando atacam outros grupos .
Quando listam suas regras e aplicam os seus castigos .
João Grande é adulto quando defende Dora , colocando os seus princípios acima de o grupo .
Professor é adulto quando a dor de um amor morto o faz partir e usufruir o seu maior dom .
Jorge Amado é mestre a o levar o leitor A as ruas de a Bahia , vivendo junto a os meninos de rua , tornando - os íntimos , mas nem sempre amigos .
Os cheiros , as dores , o ódio estão latentes em as páginas , e por isso é difícil muitas vezes lembrar que os Capitães da Areia são meninos e não homens .
A reação de quem ler vai depender de a própria experiência de vida .
É possível sentir pena , é possível se sentir indiferente , é possível se aborrecer e pensar em o por que a polícia não prende - nem em a literatura - esses delinquentes .
Resenha completa em : http://literamandoliteraturando.blogspot.com.br/2012/01/capitaes-da-areia.html
Capitães de Areia - Quem sabe o melhor livro que a Fuvest e a Unicamp mandou ler
As crianças que eram ladras dividia a opinião de as pessoas em a cidade muitos diziam que eram vagabundagens e outros já achavam que a culpa era de a própria sociedade que em a verdade nada fizera para ajuda - los , exemplo de essa última opinião era o Padre José Pedro .
Jorge Amado escreve de um modo tão lindo , tão sincero , e tão poético , que após a leitura você sente que realmente conheceu todos aqueles pobres meninos de a Bahia .
Vou embora , Barandão agora fica o chefe .
Alberto vem sempre ver vocês , vocês devem fazer o que ele diz .
E todo mundo ouça : Barandão agora é o chefe .
O negrinho Barandão fala : - Gentes , Pedro Bala vai embora .
Viva Pedro Bala !
... Os punhos de os Capitães da Areia se levantam fechados .
- Bala !
Bala !
- gritam em uma despedida .
Os gritos enchem a noite , calam a voz de o negro que canta em o mar , estremecem os céu de estrelas e o coração de Pedro .
Punhos fechados de crianças que se levantam .
Bocas que gritam se despedindo de o chefe : Bala !
Bala !
Barandão está em a frente de todos .
Ele agora é o chefe .
Pedro bala perece ver Volta Seca , Sem-Pernas , Gato , Professor , Pirulito , Boa-Vida , João Grande e Dora , todos a o mesmo tempo de entre eles .
Agora o destino de eles mudou .
