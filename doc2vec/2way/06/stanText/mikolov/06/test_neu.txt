Capitães da Areia
Capitães da Areia era um grupo de meninos que viviam de o furto em a '' cidade alta '' , em Salvador .
Eles moravam escondidos , em um '' trapiche '' , sujo e infestado de ratos .
Era difícil alguém em a cidade gostar de os Capitães , e a polícia vivia perseguindo eles , mesmo A as vezes sem motivo algum , mas é claro , existiam poucas pessoas que se relacionavam com eles , temos como exemplo , José Pedro , D. Aninha , João de Adão e Querido-de-Deus .
Com o passar de o livro uma doença chega a Salvador e mata um de os Capitães de a Areia .
Jorge Amado - Capitães de Areia
Capitães de Areia é um livro baseado em a marginalidade de crianças de rua que vivem em Salvador , mais precisamente , em um trapiche abandonado por os marinheiros .
Pedro Bala é o chefe de o grupo de os '' capitães de areia '' , em torno de ele , outros meninos , como Sem-Pernas , João Grande , Professor , Gato , Querido-de-Deus e Pirulito , Pirulito de entre outros , levando suas vidas por roubos , brigas , e acima de tudo ausência de amor e carinho .
Pedro Bala se apaixona por Dora , estes '' casam '' , e tempos depois ela fica muito doente vindo a falecer .
Resenha de o livro - Capitães De a Areia
Um trapiche - ou armazém abandonado - , a a beira-mar , que em o passado fora um local movimentado e agora está sujo e infestado de ratos .
Fora frequentado inicialmente por a marginália , até ser tomado por o bando de os Capitães de a Areia .
A o contrário de outros grupos espalhados por a cidade , os Capitães da Areia têm um líder , seguem normas e , principalmente , obedecem a um chefe que cumpre o papel de '' manter um lar '' para as crianças que ali vivem .
Pedro Bala , quase naturalmente surge como um líder e tem o papel de harmonizar , manter a ordem e , de certa maneira , ensiná - los a agir sob certas circunstâncias .
Com quinze anos , audaz , ativo e conhecedor de todos os recantos de a cidade , é marcado por uma cicatriz e por seus cabelos loiros .
Poucos lhe conheceram a mãe , e o pai '' morrera em um balaço '' .
Para firmar a liderança , Pedro Bala destituiu o caboclo Raimundo , após uma luta por o '' poder '' .
O ápice de a primeira parte vem em dois momentos : quando os meninos se envolvem com um carrossel mambembe que chegou em a cidade , e experimentam as sensações infantis ; e quando a varíola ataca a cidade e acaba matando um de eles , Almiro , apesar de a tentativa de o padre José Pedro em ajudá - los , e tendo grandes embaraços por causa de isso ... Relata a história de amor que surge quando Dora torna - se a primeira '' Capitã de a Areia '' .
Apesar de inicialmente os garotos tentarem estuprá - la , Dora vira uma espécie de mãe para eles .
Mas isto acontecera apenas uma vez !
Podemos até mesmo afirmar que por breves momentos , Dora era a única pessoa sobre a qual o líder de os Capitães de a Areia , Pedro Bala , estaria disposto a pensar a matar , e de esta forma ultrapassar o sentimento de '' Carpe Diem '' .
Bem como era uma espécie de '' estrela '' , que por sua guiou e fez sonhar Pedro Bala com simplesmente o seu amor .
Mostra a desintegração de os líderes .
Sem-Pernas se mata antes de ser capturado por a polícia que odeia ; Professor parte para o Rio de Janeiro onde torna - se um pintor de sucesso , entristecido com a morte de Dora ; Gato se torna um malandro de verdade , abandonando eventualmente sua amante Dalva , e passando por Ilhéus ; Pirulito se torna padre ; Padre José Pedro finalmente consegue uma paróquia em o interior , e vai para lá ajudar os desgarrados de o rebanho de o Sertão ; Volta Seca se torna um cangaceiro de o grupo de Lampião e mata mais de 60 soldados antes de ser capturado e condenado ; João Grande torna - se marinheiro ; Querido-de-Deus continua sua vida de capoeirista e malandro ; Pedro Bala , cada vez mais fascinado com as histórias de seu pai sindicalista , vai se envolvendo com os doqueiros e finalmente os Capitães da Areia ajudam em uma greve .
Pedro Bala abandona a liderança de o grupo , transferindo - a para Barandão , mas antes os transforma em uma espécie de grupo de choque .
Assim Pedro Bala deixa de ser o líder de os Capitães da Areia e se torna um líder revolucionário comunista .
Capitães da Areia - Jorge Amado
Os Capitães da Areia conta sobre um grupo de meninos de rua .
O livro é dividido em três partes .
Antes de elas , explica - se que os '' Capitães de a Areia '' é um grupo de menores abandonados e marginalizados .
Os únicos que se relacionam com eles são Padre José Pedro e uma mãe-de-santo .
O Reformatório é cruel , e eles querem ficar longe de isso e a polícia caçam - nos .
A primeira parte , conta algumas histórias sobre alguns de os principais Capitães da Areia - o grupo era grande , eles viviam em um trapiche e tinha líder - .
Pedro Bala , o líder , uma espécie de pai para os garotos , mesmo sendo tão jovem quanto os outros , e depois descobre ser filho de um líder sindical morto durante uma greve .
Volta Seca tem ódio de as autoridades e o desejo de se tornar cangaceiro ; Professor , que lê e desenha perfeitamente e é muito talentoso ; Gato , que com seu jeito malandro acaba conquistando Dalva ; Sem-Pernas , o garoto que se finge de órfão - e vai em uma casa que é bem acolhido , mas trai a família - .
João Grande , o '' negro bom '' ; Padre José Pedro tentando ajudá - los e se colocando em fria por isso .
A segunda parte , '' Noite da Grande Paz , de a Grande Paz de os teus olhos '' , surge uma história de amor quando a menina Dora torna - se a primeira '' Capitã de a Areia '' , ela se torna como mãe e irmã para todos .
Professor e Pedro bala se apaixonam por ela , e Dora se apaixona por Pedro Bala .
Quando Pedro e ela são capturados e ela em pouco tempo passa a roubar também , eles são muito castigados , vão para o Reformatório e Orfanato .
Quando escapam , se amam por a primeira vez em a praia e ela morre .
'' Canção de a Bahia , Canção de a Liberdade '' , a terceira parte , vai em os mostrando a desintegração de os líderes .
Sem-Pernas se mata ; Professor parte para o RJ para se tornar um pintor de sucesso ; Pirulito se torna frade ; Padre José Pedro finalmente consegue uma paróquia em o interior , e vai para lá ajudar ; Volta Seca se torna um cangaceiro de o grupo de Lampião e mata mais de 60 soldados antes de ser capturado e condenado ; João Grande torna - se marinheiro ; Querido-de-Deus continua sua vida de capoeirista e malandro ; Pedro Bala , cada vez mais fascinado com as histórias de seu pai sindicalista , vai se envolvendo com os doqueiros e finalmente os Capitães da Areia ajudam em uma greve .
Pedro Bala abandona a liderança de o grupo , mas antes os transforma em uma espécie de grupo de choque .
Assim Pedro Bala deixa de ser o líder de os Capitães de Areia e se torna um líder revolucionário comunista .
Este livro foi escrito em a primeira fase de a carreira de Jorge Amado , e nota - se grandes preocupações sociais .
As autoridades e o clero são sempre retratados como opressores - Padre José Pedro é uma exceção ; antes de ser um bom padre foi um operário - , cruéis e responsáveis por os males .
Resenha '' Capitães de a Areia ''
O livro de Jorge Amado , retrata bem a realidade de meninos de rua em Salvador , eles eram malandros e tinham a inteligencia de homens mais velhos , eles não tinham nomes , eles tinham apelidos por suas devidas caracteristicas , o livro conta com os principais personagens , Gato , Boa-vida , Sem-Pernas , João Grande , Querido-de-Deus , Pirulito , Dora , Raimundo , Don ` Aninha , Padre Jose Pedro , Professor , Pedro Bala , João de Adão e Barandão .
A historia é uma aventura , que conta como eles sobrevivem em o dia-a-dia , e lidam com as dificuldades de morar em a rua !
Em o desenrrolar de o livros , os meninos se metem em confusões e dificuldades , onde nota - se preocupações sociais de o autor .
É visível , que Jorje Amado não quis fazer uma crítica a os meninos , e sim mostrar a todos como é a vida em as ruas e tudo que os meninos têm que passar .
A pesar de o grupo de marginalizados cometerem crimes , não consegui enxerga - os como vilões , pessoas de o mal , e sim como crianças que se vêm obrigadas a fazerem isso pra a sua sobrevivência .
O livro é dividido em três partes .
Antes de elas tem uma serie de reportagens e depoimentos , explicando que os Capitães da Areia é um grupo de menores abandonados .
O nome de o livro surgiu porque o grupo de marginalizados moram eum um trapiche em a areia !
Capitaes de a areia e um livro escrito por gorge amado e um clasico literatra brasilera ela se passa em a bahia são um grupo de criansas abandonadas q se juntam para poder sobreviver fazendo vandalismo e sendo procurados por a policia e sendo ajudados por um padre O livro se divide em três partes : Primeira Parte : Sob a lua , em um velho trapiche abandonado Segunda parte : Noite da Grande Paz , de a Grande Paz de os teus olhos Terceira parte : Canção de a Bahia , Canção da Liberdade
Resenha - Capitães da Areia
