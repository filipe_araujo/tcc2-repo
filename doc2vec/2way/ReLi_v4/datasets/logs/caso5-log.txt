-----------------------tags inferidas-----------------------
[ 1.  1.  1.  1.  1.  0.  1.  0.  1.  1.  1.  1.  1.  1.  1.  1.  0.  1.
  1.  1.  1.  1.  1.  0.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  0.
  0.  1.  1.  1.  1.  1.  0.  0.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.
  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  0.  1.  1.  1.  1.  1.  1.  1.
  0.  1.  1.  1.  1.  1.  0.  1.  1.  1.  1.  1.  0.  1.  1.  1.  1.  1.
  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  1.  0.  1.  0.
  1.  1.  1.  0.  1.  1.  1.  1.  1.  1.]
--------------------------------------------------------------
1 | 51 | 52
0 | 7 | 8
 0.508475 : 20 passes : dbow+dmc_inferred 0.0s 1.8s
completed pass 20 at alpha 0.002200
END 2015-11-22 18:16:28.911045
0.389831 dbow+dmc_inferred
0.406780 Doc2Vec(dbow,d100,n5,mc2,t8)_inferred
0.432203 dbow+dmm_inferred
0.440678 Doc2Vec(dbow,d100,n5,mc2,t8)
0.440678 Doc2Vec(dm/c,d100,n5,w5,mc2,t8)
0.440678 dbow+dmc
0.457627 dbow+dmm
0.474576 Doc2Vec(dm/m,d100,n5,w10,mc2,t8)
0.474576 Doc2Vec(dm/m,d100,n5,w10,mc2,t8)_inferred
0.483051 Doc2Vec(dm/c,d100,n5,w5,mc2,t8)_inferred
for doc 1604...
Doc2Vec(dm/c,d100,n5,w5,mc2,t8):
 [(1282, 0.5896920561790466), (251, 0.5704914927482605), (1248, 0.5549823045730591)]
Doc2Vec(dbow,d100,n5,mc2,t8):
 [(56, 0.9704635143280029), (1216, 0.9684956669807434), (1551, 0.9672698378562927)]
Doc2Vec(dm/m,d100,n5,w10,mc2,t8):
 [(775, 0.7251611351966858), (1289, 0.7226477861404419), (1218, 0.7155406475067139)]
TARGET (380): «Demorei uma semana para lê - lo , li com calma , mas foi uma leitura super cativante .»

SIMILAR/DISSIMILAR DOCS PER MODEL Doc2Vec(dm/m,d100,n5,w10,mc2,t8):

MOST (1064, 0.8608391284942627): «Por vezes me vi cansada , exaurida mesmo , após ler apenas 15 ou 20 páginas , não por ficar com a vista cansada nem nada de o tipo , e sim por pensar .»

MEDIAN (440, 0.41966384649276733): «Um de os melhores livros que já li .»

LEAST (1206, -0.3251628577709198): «Simplesmente mais um livro de o gênero seção de a tarde , nada mais , nada menos .»

most similar words for '?' (20 occurences)
