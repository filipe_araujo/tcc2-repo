sentences = []
doc_neutral = []
doc_pos = []
doc_neg = []

with open('reli_text.txt') as text:
    for line_sent in text:
        sentences.append(line_sent)

polarity = []
with open('reli_pol.txt') as pol:
    for line_pol in pol:
        if line_pol[0] == 'O':
            polarity.append(0)
        elif line_pol[0] == '+':
            polarity.append(1)
        elif line_pol[0] == '-':
            polarity.append(-1)

for s,p in zip(sentences,polarity):
    if p == 0:
        doc_neutral.append(s)
    elif p == 1:
        doc_pos.append(s)
    elif p == -1:
        doc_neg.append(s)

print(len(doc_pos))
print(len(doc_neg))
print(len(doc_neutral))


with open('reli_pos.txt', 'w') as reli_pos:
    for doc in doc_pos:
        reli_pos.write(doc)

with open('reli_neg.txt', 'w') as reli_neg:
    for doc in doc_neg:
        reli_neg.write(doc)

with open('reli_neu.txt', 'w') as reli_neutral:
    for doc in doc_neutral:
        reli_neutral.write(doc)