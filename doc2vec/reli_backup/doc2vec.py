__author__ = 'filipe'

import multiprocessing
import os.path
assert os.path.isfile("/home/filipe/Workspace/tcc2-repo/doc2vec/reli/datasets/train-test_pos/caso1-id.txt"), "caso1-id.txt unavailable"
assert os.path.isfile("/home/filipe/Workspace/tcc2-repo/doc2vec/reli/datasets/train-test_neg/caso1-id.txt"), "caso1-id.txt unavailable"
import gensim
import gensim.models.doc2vec
from gensim.models import Doc2Vec
from gensim.models.doc2vec import TaggedDocument
from collections import namedtuple
from collections import OrderedDict
from collections import defaultdict
from gensim.test.test_doc2vec import ConcatenatedDoc2Vec
import numpy as np
import statsmodels.api as sm
import random
from random import sample
from contextlib import contextmanager
from timeit import default_timer
import time
from random import shuffle
from IPython.display import HTML
import datetime

SentimentDocument = namedtuple('SentimentDocument', 'words tags split sentiment')
alldocs = []

posdocs = []  # will hold positive docs in original order
with open('/home/filipe/Workspace/tcc2-repo/doc2vec/reli/datasets/train-test_pos/caso1-id.txt') as posdata:
    for line_no, line in enumerate(posdata):
        tokens_pos = gensim.utils.to_unicode(line).split()
        words_pos = tokens_pos[1:]
        tags_pos = [line_no] # `tags = [tokens[0]]` would also work at extra memory cost #
        split_pos = ['train','test'][line_no//2567]
        sentiment_pos = 1.0
        posdocs.append(SentimentDocument(words_pos, tags_pos, split_pos, sentiment_pos))

pos_train_docs = [doc for doc in posdocs if doc.split == 'train']
pos_test_docs = [doc for doc in posdocs if doc.split == 'test']

negdocs = []
with open('/home/filipe/Workspace/tcc2-repo/doc2vec/reli/datasets/train-test_neg/caso1-id.txt') as negdata:
    for line_no, line in enumerate(negdata):
        tokens_neg = gensim.utils.to_unicode(line).split()
        words_neg = tokens_neg[1:]
        tags_neg = [line_no] # `tags = [tokens[0]]` would also work at extra memory cost
        split_neg = ['train','test'][line_no//532]
        sentiment_neg = 0.0
        negdocs.append(SentimentDocument(words_neg, tags_neg, split_neg, sentiment_neg))

neg_train_docs = [doc for doc in negdocs if doc.split == 'train']
neg_test_docs = [doc for doc in negdocs if doc.split == 'test']

#print ("neg train docs")
#print (neg_train_docs)

#print ("neg test docs")
#print (neg_test_docs)

train_docs = pos_train_docs + neg_train_docs
test_docs = pos_test_docs + neg_test_docs

#train_docs = [doc for doc in posdocs and negdocs if doc.split == 'train']
#test_docs = [doc for doc in posdocs and negdocs if doc.split == 'test']

#print(train_docs)
#print(test_docs)

alldocs = train_docs + test_docs
doc_list = alldocs[:]  # for reshuffling per pass

print('%d docs: %d train-sentiment, %d test-sentiment' % (len(doc_list), len(train_docs), len(test_docs)))

cores = multiprocessing.cpu_count()
assert gensim.models.doc2vec.FAST_VERSION > -1, "this will be painfully slow otherwise"

simple_models = [
    # PV-DM w/concatenation - window=5 (both sides) approximates paper's 10-word total window size
    Doc2Vec(dm=1, dm_concat=1, size=100, window=5, negative=5, hs=0, min_count=2, workers=cores),
    # PV-DBOW
    Doc2Vec(dm=0, size=100, negative=5, hs=0, min_count=2, workers=cores),
    # PV-DM w/average
    Doc2Vec(dm=1, dm_mean=1, size=100, window=10, negative=5, hs=0, min_count=2, workers=cores),
]

# speed setup by sharing results of 1st model's vocabulary scan
simple_models[0].build_vocab(alldocs)  # PV-DM/concat requires one special NULL word so it serves as template
print(simple_models[0])
for model in simple_models[1:]:
    model.reset_from(simple_models[0])
    print(model)

models_by_name = OrderedDict((str(model), model) for model in simple_models)

models_by_name['dbow+dmm'] = ConcatenatedDoc2Vec([simple_models[1], simple_models[2]])
models_by_name['dbow+dmc'] = ConcatenatedDoc2Vec([simple_models[1], simple_models[0]])

@contextmanager
def elapsed_timer():
    start = default_timer()
    elapser = lambda: default_timer() - start
    yield lambda: elapser()
    end = default_timer()
    elapser = lambda: end-start

def logistic_predictor_from_data(train_targets, train_regressors):
    logit = sm.Logit(train_targets, train_regressors)
    predictor = logit.fit(disp=0)
    #print(predictor.summary())
    return predictor

def error_rate_for_model(test_model, train_set, test_set, infer=False, infer_steps=3, infer_alpha=0.1, infer_subsample=0.1):
    """Report error rate on test_doc sentiments, using supplied model and train_docs"""

    train_targets, train_regressors = zip(*[(doc.sentiment, test_model.docvecs[doc.tags[0]]) for doc in train_set])
    train_regressors = sm.add_constant(train_regressors)
    predictor = logistic_predictor_from_data(train_targets, train_regressors)

    test_data = test_set
    #print(test_data)

    #filipe
    test_data_sentiment = []

    #for (dota=0dota<344;dota=dota+1):
    #for dota in test_data:
    dota = 0;
    while dota<344:
        test_data_sentiment.append(test_data[dota].sentiment)
        #print(test_data[dota].sentiment)
        dota = dota + 1
    print(test_data_sentiment)

    if infer:
        if infer_subsample < 1.0:
            test_data = sample(test_data, int(infer_subsample * len(test_data)))
        test_regressors = [test_model.infer_vector(doc.words, steps=infer_steps, alpha=infer_alpha) for doc in test_data]
    else:
        test_regressors = [test_model.docvecs[doc.tags[0]] for doc in test_docs]
    test_regressors = sm.add_constant(test_regressors)

    # predict & evaluate
    test_predictions = predictor.predict(test_regressors)
    corrects = sum(np.rint(test_predictions) == [doc.sentiment for doc in test_data])
    errors = len(test_predictions) - corrects
    error_rate = float(errors) / len(test_predictions)
    print ("-----------------------tags inferidas-----------------------")
    print(np.rint(test_predictions))
    print ("--------------------------------------------------------------")
    return (error_rate, errors, len(test_predictions), predictor)

best_error = defaultdict(lambda :1.0)

alpha, min_alpha, passes = (0.025, 0.001, 20)
alpha_delta = (alpha - min_alpha) / passes

print("START %s" % datetime.datetime.now())

for epoch in range(passes):
    shuffle(doc_list)  # shuffling gets best results

    for name, train_model in models_by_name.items():
        # train
        duration = 'na'
        train_model.alpha, train_model.min_alpha = alpha, alpha
        with elapsed_timer() as elapsed:
            train_model.train(doc_list)
            duration = '%.1f' % elapsed()

        # evaluate
        eval_duration = ''
        with elapsed_timer() as eval_elapsed:
            err, err_count, test_count, predictor = error_rate_for_model(train_model, train_docs, test_docs)
        eval_duration = '%.1f' % eval_elapsed()
        best_indicator = ' '
        if err <= best_error[name]:
            best_error[name] = err
            best_indicator = '*'
        print("%s%f : %i passes : %s %ss %ss" % (best_indicator, err, epoch + 1, name, duration, eval_duration))

        if ((epoch + 1) % 5) == 0 or epoch == 0:
            eval_duration = ''
            with elapsed_timer() as eval_elapsed:
                infer_err, err_count, test_count, predictor = error_rate_for_model(train_model, train_docs, test_docs, infer=True, infer_subsample=1)
            eval_duration = '%.1f' % eval_elapsed()
            best_indicator = ' '
            if infer_err < best_error[name + '_inferred']:
                best_error[name + '_inferred'] = infer_err
                best_indicator = '*'
            print("%s%f : %i passes : %s %ss %ss" % (best_indicator, infer_err, epoch + 1, name + '_inferred', duration, eval_duration))

    print('completed pass %i at alpha %f' % (epoch + 1, alpha))
    alpha -= alpha_delta

print("END %s" % str(datetime.datetime.now()))

# print best error rates achieved
for rate, name in sorted((rate, name) for name, rate in best_error.items()):
    print("%f %s" % (rate, name))

doc_id = np.random.randint(simple_models[0].docvecs.count)  # pick random doc; re-run cell for more examples
print('for doc %d...' % doc_id)
for model in simple_models:
    inferred_docvec = model.infer_vector(alldocs[doc_id].words)
    print('%s:\n %s' % (model, model.docvecs.most_similar([inferred_docvec], topn=3)))



doc_id = np.random.randint(simple_models[0].docvecs.count)  # pick random doc, re-run cell for more examples
model = random.choice(simple_models)  # and a random model
sims = model.docvecs.most_similar(doc_id, topn=model.docvecs.count)  # get *all* similar documents
print(u'TARGET (%d): «%s»\n' % (doc_id, ' '.join(alldocs[doc_id].words)))
print(u'SIMILAR/DISSIMILAR DOCS PER MODEL %s:\n' % model)
for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
    print(u'%s %s: «%s»\n' % (label, sims[index], ' '.join(alldocs[sims[index][0]].words)))


# pick a random word with a suitable number of occurences
word_models = simple_models[:]
while True:
    word = random.choice(word_models[0].index2word)
    if word_models[0].vocab[word].count > 10:
        break
# or uncomment below line, to just pick a word from the relevant domain:
#word = 'comedy/drama'
similars_per_model = [str(model.most_similar(word, topn=20)).replace('), ','),<br>\n') for model in word_models]
similar_table = ("<table><tr><th>" +
    "</th><th>".join([str(model) for model in word_models]) +
    "</th></tr><tr><td>" +
    "</td><td>".join(similars_per_model) +
    "</td></tr></table>")
print("most similar words for '%s' (%d occurences)" % (word, simple_models[0].vocab[word].count))
HTML(similar_table)